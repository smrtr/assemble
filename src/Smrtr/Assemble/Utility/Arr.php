<?php

namespace Smrtr\Assemble\Utility;

use Closure;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @supackage Utility\Arr
 */
class Arr
{
	/**
	 * Wrap
	 * @param array $array
	 * @return array
	 */
	public function wrap(array $array = array(), $wrap = "'")
	{
		return array_map(function($var) use($wrap) {
			return is_string($var)? $wrap.trim($var).$wrap: $var;
		}, $array);
	}

	/**
	 * Trim array
	 * @param array $array
	 * @param string $delimeter
	 * @return array
	 */
	public function trim(array $array, $delimeter = '')
	{
		return array_map(function($val) use($delimeter) { return trim($val, $delimeter);}, $array);
	}

	/**
	 * Apply
	 * @param array $array
	 * @param Closure $callback
	 * @return array
	 */
	public function apply(array $array, Closure $callback)
	{
		return array_map(function($val) use($callback) {return $callback($val);}, $array);
	}

	/**
	 * Join
	 * @param array $array
	 * @param string $delimeter
	 * @return array
	 */
	public function join(array $array, $delimeter = '')
	{
		return implode($delimeter, $array);
	}

	/**
	 * Make path
	 * @param string $parts
	 * @return array
	 */
	public function toPath(array $parts)
	{
		$params = array(null);
		foreach($parts as $param) {
			if(is_string($param) && $param) $params[] = $param;
		}

		return $this->join($this->trim($params, '/'), '/');
	}

}