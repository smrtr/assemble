<?php

namespace Smrtr\Assemble\Utility;

use Closure;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @supackage Utility\File
 */
class File
{
	/**
	 * Get directory contents
	 *
	 * @param string $dir
	 * @return array
	 */
	protected function getDirectoryFiles($dir)
	{
		return file_exists($dir)? scandir($dir) :array();
	}

	/**
	 * Scan dir
	 *
	 * Given a path, recurrsively store items into object
	 *
	 * @param string $directory
	 */
	public function getFiles($directory, Closure $predicate = null)
	{
		$files = array();
		$directory = substr($directory, -1) === '/'? substr($directory, 0, -1): $directory;

		if(is_dir($directory))
		{
			foreach($this->getDirectoryFiles($directory) as $file)
			{

				if((substr($file, 0, 1) !== '.'))
				{
					$path = $directory.'/'.$file;

					if(is_dir($path))
					{
						$files = array_merge($files, $this->getFiles($path, $predicate));
					}
					else if(is_file($path))
					{
						if($predicate !== null) {
							if($predicate(pathinfo($path))) $files[] = $path;
						}else $files[] = $path;
					}
				}
			}
		}

		return $files;
	}

	/**
	 * Get path info
	 * @param string $path
	 * @param string $type
	 * @return array
	 */
	public function info($path, $type = null)
	{
		$info = pathinfo($path);

		if($type) return array_key_exists($type, $info)? $info[$type]: null;

		return $info;
	}

}