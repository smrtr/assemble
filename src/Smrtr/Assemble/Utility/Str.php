<?php

namespace Smrtr\Assemble\Utility;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @supackage Utility\String
 */
class Str
{
	/**
	 * Pascal Case
	 * @param  string $string
	 * @return string
	 */
	public function pascalCase($string)
	{
		return preg_replace("/[\s]+/",'', ucwords($this->split($string)));
	}

	/**
	 * Camel Case
	 * @param  string $string
	 * @return string
	 */
	public function camelCase($string)
	{
		return lcfirst($this->pascalCase($string));
	}

	/**
	 * Underscore string
	 * @param  string $string
	 * @return string
	 */
	public function underscore($string)
	{
		return strtolower($this->split($string, '_'));
	}

	/**
	 * Snake Case
	 * @param  string $string
	 * @return string
	 */
	public function snakeCase($string)
	{
		return strtolower($this->split($string, '-'));
	}

	/**
	 * Title
	 * @param  string $string
	 * @return string
	 */
	public function title($string)
	{
		return ucwords($this->split($string));
	}

	/**
	 * Starts with
	 * @param string $stack
	 * @param string $needle
	 * @return boolean
	 */
	public function startsWith($stack, $needle)
	{
		return $needle === substr($stack, 0, strlen($needle));
	}

	/**
	 * Difference
	 * @param string $string
	 * @param
	 * @return string
	 */
	public function difference($stack, $needle)
	{
		$nl	= strlen($needle);

		if($stack === $needle) return null;

		if(($needle === substr($stack, $nl *- 1)) && ($return = substr_replace($stack, '', $nl *- 1))){
			return $return;
		};

		if(($needle === substr($stack, 0, $nl)) && ($return = substr_replace($stack, '', 0, $nl))){
			return $return;
		};

		return $stack;
	}

	/**
	 * Split string
	 * @param  string $string
	 * @param  string $del delimeter
	 * @return string
	 */
	public function split($string, $del = ' ')
	{
		return trim(preg_replace("/[\-\_\.\s]+/", $del, preg_replace("/[A-Z]+/", $del.'$0', $string)), '- ._');
	}
}