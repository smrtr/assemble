<?php

namespace Smrtr\Assemble;

use Closure;
use Smrtr\Assemble\Container as Services;
use Smrtr\Assemble\Syntax;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @subpackage ConfigureAbstract
 */
abstract class ConfigureAbstract
{
	/**
	 * @var array Resolve configs
	 */
	protected $config = array();

	/**
	 * @var array Required
	 */
	protected $required = array();

	/**
	 * @var array visible
	 */
	protected $visible = array();

	/**
	 * Resolve configs
	 * @return void
	 */
	abstract protected function resolveConfigs();


	/**
	 * Resolve configs
	 * @param array $config
	 * @return void
	 */
	public function configure(array $config = array())
	{
		$missing = array_diff($this->required, array_keys($config));
		if(count($missing)) {
			throw new \Exception(implode('|', $missing).' required and missing');
		}
		$this->config = $config;
		$this->resolveConfigs();
		return $this;
	}

	/**
	 * Get visible properties
	 * @return array
	 */
	public function getVisibleProperties()
	{
		$visible = array();
		foreach($this->visible as $item)
		{
			$visible[$item] = null;
			if(property_exists($this, $item)) {
				$visible[$item] = $this->{$item};
			}
		}
		return $visible;
	}

	/**
	 * Get visible properties
	 * @param string $property
	 * @return mixed
	 */
	public function __get($property)
	{
		return property_exists($this, $property) && in_array($property, $this->visible)? $this->{$property}: null;
	}
	/**
	 * Get Config
	 * @param string $config
	 * @param mixed $value
	 * @return mixed
	 */
	public function config($config, $value = null)
	{
		if($value !== null && $config) {
			$this->config[$config] = $value;
		}
		else return array_key_exists($config, $this->config)? $this->config[$config]: false;
	}

	/**
	 * Get Configs
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}


}