<?php

namespace Smrtr\Assemble;

use Closure;
use Smrtr\Assemble\Syntax;
use Smrtr\Assemble\Component;
use Smrtr\Assemble\Dependency;
use Smrtr\Assemble\Annotation;
use Smrtr\Assemble\Utility\Arr;
use Smrtr\Assemble\Utility\Str;
use Smrtr\Assemble\Utility\File;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @subpackage Service
 */
class Container
{
	protected $container = array();

	public function __construct() {

		$this->add('dependency', function($c){
			return new Dependency($c->array);
		});

		$this->add('array', function($c){
			return new Arr;
		});

		$this->add('string', function($c){
			return new Str;
		});

		$this->add('annotation', function($c){
			return new Annotation($c->dependency);
		});

		$this->add('syntax', function($c){
			return new Syntax($c);
		});

		$this->add('file', function($c){
			return new File;
		});

		$this->add('component', function($c){
			return new Component($c);
		});
	}

	/**
	 * Add
	 * @param string $key
	 * @param closure $factory
	 * @return void
	 */
	public function add($key, Closure $factory)
	{
		if($key) return $this->container[$key] = $factory;
		throw Exception('Unable to add Key required');

		return $this;
	}

	/**
	 * Get
	 * @param string $service
	 * @return object
	 */
	public function __get($service)
	{
		if(array_key_exists($service, $this->container) && $this->container[$service] instanceof Closure)
		{
			return $this->container[$service]($this);
		}
	}
}