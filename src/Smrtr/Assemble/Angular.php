<?php

namespace Smrtr\Assemble;

use Smrtr\Assemble\Container;
use Smrtr\Assemble\ConfigureAbstract;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @subpackage App
 */
class Angular extends ConfigureAbstract
{
	/**
	 * @var string
	 */
	protected $module;

	/**
	 * @var array
	 */
	protected $required = array('app_path', 'pub_path');


	/**
	 * @var array
	 */
	protected $visible  = array('routes', 'dialogs');

	/**
	 * @var array
	 */
	protected $routes  = [];

	/**
	 * @var array
	 */
	protected $dialogs = [];

	/**
	 * @var array
	 */
	protected $models  = [];

	/**
	 * @var array
	 */
	protected $packages = [];

	/**
	 * @var array
	 */
	protected $scripts = [];

	/**
	 * @var array
	 */
	protected $content = [];

	/**
	 * @var array
	 */
	protected $paths = [];

	/**
	 * @var array
	 */
	protected $properties = [];

	/**
	 * @var array
	 */
	protected $handlers = [];

	/**
	 * @var \Smrtr\Assemble\Container
	 */
	protected $service;

	/**
	 * Get module
	 * @access private
	 * @param string $module
	 */
	public function __construct(array $configs = [], array $handlers = [])
	{
		$this->service = new Container;
		$this->configure($configs);
		$this->handlers = $handlers;
		$this->compile();
	}

	/**
	 * Resolve Configs
	 * @return void
	 */
	protected function resolveConfigs()
	{
		if(!$this->service->string->startsWith(
				$this->config('app_path'),
				$this->config('pub_path')
			)
		) {
			$this->config('app_path',
				$this->service->array->toPath(array(
					$this->config('pub_path'),
					$this->config('app_path'))
				)
			);
		}

		$this->config('url_path',
			$this->service->array->toPath(array(
				$this->service->string->difference(
					$this->config('app_path'),
					$this->config('pub_path')
				)
			))
		);

		if($this->config('app_path') && !$this->config('app_name')) {
			$this->config('app_name', end(explode('/', trim($this->config('app_path'), '/'))));
		}
	}

	/**
	 * Set component aliases
	 * @param  string $content
	 * @return mixed
	 */
	public function setComponentAliases(array $aliases)
	{
		$this->service->component->setComponentAliases($aliases);
	}

	/**
	 * Push content
	 * @param  string $content
	 * @return mixed
	 */
	public function push($type, $content, $key = false)
	{
		if(property_exists($this, $type))
		{
			if($key) return $this->{$type}[$key] = $content;

			if(is_string($content)) return $this->{$type}[$content] = $content;

			return $this->{$type}[] = $content;
		}
	}

	/**
	 * Packages
	 * @access public
	 * @param  array $packages
	 * @return array
	 */
	public function modules($packages = [])
	{	
		foreach($packages as $package) {
			if($package instanceof Angular) {
				$this->addModule($package);
			}
			else $this->packages[] = $package;
		}
	}

	/**
	 * Compile routes
	 *
	 * @access private
	 * @return string
	 */
	protected function runCallbacks()
	{
		foreach($this->handlers as $handle => $callback)
		{
			if($callback instanceof \Closure)
			{
				$this->content[$handle] = $callback($this);
			}
		}
	}

	/**
	 * Compile routes
	 *
	 * @access private
	 * @return string
	 */
	public function compile()
	{
		$files = $this->service->file->getFiles($this->config('app_path'), function($info) {
			return (in_array($info['extension'], array('js', 'php')) && '_' != substr($info['filename'], 0, 1));
		});

		foreach($files as $path)
		{
			$component = $this->service->component->configure(array(
				'app_name' => $this->config('app_name'),
				'pub_path' => $this->config('pub_path'),
				'app_path' => $this->config('app_path'),
				'src_path' => $path
			));

			$this->content[$component->module] = $component->output();

			if(method_exists($this, ($method = 'compileProvider'.ucwords($component->alias))))
			{
				$this->{$method}($component);
			}
		}
		return $this;
	}

	/**
	 * Configure paths
	 *
	 * @access private
	 * @param boolean $initialise
	 * @return void
	 */
	protected function configurePaths(array $script)
	{
		$key = ($script['namespace'] === $script['module'])? $script['module']:$script['namespace'].'_'.$script['module'];

        $this->paths[$key] = array(
            'module'   => $script['baseurl'],
            'partials' => $script['baseurl'].'/partials',
            'pages'    => $script['baseurl'].'/pages',
            'dialogs'  => $script['baseurl'].'/dialogs',
        );
	}

	/**
	 * Output
	 *
	 * @access private
	 * @param boolean $initialise
	 * @return string
	 */
	public function addModule(Angular $angular)
	{
		$this->content = array_merge($angular->getContent(), $this->content);
	}

	/**
	 * Output
	 *
	 * @access private
	 * @param boolean $initialise
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Output
	 *
	 * @access private
	 * @param boolean $initialise
	 * @return string
	 */
	public function output($new = true)
	{
		$this->runCallbacks();
		return ($new? $this->service->syntax->constructor($this->config('app_name'), $this->packages): null).";\n" . implode("\n", $this->content);
	}

	/**
	 * Object has property
	 *
	 * @param  string  $type of
	 * @return boolean
	 */
	public function has($type)
	{
		return property_exists($this, $type)? (bool)count($this->{$type}): false;
	}

	/**
	 * Compile Routes
	 * @param Provider $provider
	 * @return $this
	 */
	protected function compileProviderPage(Component $component)
	{	
		foreach($component->routes as $i => $url)
		{
			$this->routes[$url.$i] = array(
				'templateUrl' => $component->templateUrl,
				'url'		  => $url,
				'controller'  => $component->module,
				'as' 		  => $component->shortname,
				'title'       => $component->title
			);
		}
		return $this;
	}

	/**
	 * Compile Dialogs
	 * @param Provider $provider
	 * @return void
	 */
	protected function compileProviderDialog(Component $component)
	{
		$this->dialogs[$component->shortname] = array(
			'templateUrl' => $component->templateUrl,
			'controller'  => $component->module,
			'as' 		  => $component->shortname
		);
		return $this;
	}

	/**
	 * Compile Models
	 * @param Provider $provider
	 * @return void
	 */
	protected function compileProviderModel(Component $component)
	{
		$this->models[$component->module] = 'models.'.lcfirst($component->module)." = $component->module;";
	}
}