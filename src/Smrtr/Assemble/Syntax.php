<?php

namespace Smrtr\Assemble;

use Smrtr\Assemble\Container as Services;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @subpackage Syntax
 */
class Syntax
{
	/**
	 * @var \Smrtr\Assemble\Container
	 */
	protected $services;

	public function __construct(Services $services)
	{
		$this->services = $services;
	}

	/**
	 * Compiler
	 * @param string $name of component to compile
	 * @param string $format
	 * @return string
	 */
	public function __call($name, $args)
	{
		$method = 'compile'. ucwords($name);

		$component = array_shift($args);

		if(!($component instanceof Component)) {
			throw new \Exception('Smrtr\Assemble\Component not set.');
		}

		if(method_exists($this, $method))
		{
			return $this->{$method}($component);

		}
		else throw new \Exception($name." does not own a handler.");
	}

	/*
	 |	Compilers
	 |
	 |
	 */

	/**
	 * Compile Constructor
	 * @return string|array
	 */
	public function constructor($app, array $dependencies = array())
	{
		$dependency = $this->services->dependency->build($dependencies);

		if(!$app || !is_string($app)) throw \Exception('App name must be a string and not null');

		return "angular.module('{$app}',[{$dependency->parameters(true, true)}])";
	}

	/**
	 * Compile Namespaced
	 * @return string
	 */
	protected function compileNamespaced(Component $c)
	{
		$annotations = ($annotations = $c->dependencies->annotations(true))? $annotations.',': '';
		
		switch($c->component)
		{
			case 'value':
			case 'constant':
				return "angular.module('{$c->app}').{$c->component}('{$c->module}',{$c->content});";
			case 'run':
			case 'config':
				return "angular.module('{$c->app}').{$c->component}([{$annotations}function({$c->dependencies->parameters(true)}){\n{$c->content}\n}]);";
			default:
				return "angular.module('{$c->app}').{$c->component}('{$c->module}',[{$annotations}function({$c->dependencies->parameters(true)}){\n{$c->content}\n}]);";
		}
	}


}