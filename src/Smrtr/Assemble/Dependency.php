<?php

namespace Smrtr\Assemble;

use Smrtr\Assemble\Utility\Arr;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @subpackage Dependency
 */
class Dependency
{
	/**
	 * @var \Smrtr\Assemble\Utility\Arr
	 */
	protected $array;

	/**
	 * @var array $dependencies
	 */
	protected $dependencies = array();

	/**
	 * @param \Smrtr\Assemble\Utility\Arr $array
	 * @param array $dependencies
	 * @return mixed
	 */
	public function __construct(Arr $array, array $dependencies = array())
	{
		$this->array = $array;
		$this->build($dependencies);
	}

	/**
	 * Build
	 * @param array $dependencies
	 * @return \Smrtr\Assemble\Dependency
	 */
	public function build(array $dependencies)
	{
		foreach ($dependencies as $annotation => $parameter)
		{
			$combination = $this->resolveAnnotationParameter($annotation, $parameter);
			$this->dependencies[$combination->annotation] = $combination->parameter;
		}
		return $this;
	}

	/**
	 * Resolve Annotation Parameter
	 * @param string $annotation
	 * @param string $parameter
	 * @return mixed
	 */
	public function resolveAnnotationParameter($annotation, $parameter)
	{
		//pre_dump($parameter);
		if($parameter && count($value = preg_split('/[\s]+as[\s]+/i', $parameter)) === 2)
		{
			$annotation = trim($value[0]);
			$parameter  = trim($value[1]);
		}

		if(is_int($annotation) || !$annotation) {
			$annotation = $parameter;
		}

		return (object)array('annotation' => trim($annotation), 'parameter' => trim($parameter));
	}

	/**
	 * Annotations
	 * @param boolean $string
	 * @return array
	 */
	public function annotations($string = false)
	{
		$result = $this->array->wrap(array_flip($this->dependencies), "'");

		if($string === true) {
			$result = $this->array->join($result, ',');
		}

		return $result;
	}

	/**
	 * Parameters
	 * @param boolean $string
	 * @return mixed
	 */
	public function parameters($string = false, $escaped = false)
	{
		$result = $escaped? $this->array->wrap($this->dependencies, "'"): $this->dependencies;

		if($string === true) {
			$result = $this->array->join($result, ',');
		}

		return $result;
	}

	/**
	 * Non Annotated Parameters
	 * @param boolean $string
	 * @return mixed
	 */
	public function nonAnnotatedParameters($string = false)
	{
		$result = array_flip($this->dependencies);

		if($string === true) {
			$result = $this->array->join($result, ',');
		}

		return $result;
	}

	/**
	 * All injectables
	 * @return array
	 */
	public function injectables()
	{
		return $this->dependencies;
	}

}