<?php

namespace Smrtr\Assemble;

use Closure;
use Smrtr\Assemble\Container as Services;
use Smrtr\Assemble\Syntax;
use Smrtr\Assemble\ConfigureAbstract;
/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble\Component
 * @subpackage Component
 */
class Component extends ConfigureAbstract
{
	/**
	 * @var array Component conversions
	 */
	protected $compositeExtensions = array('js.php');

	/**
	 * @var string $services
	 */
	protected $services;

	/**
	 * @var string Component Name
	 */
	protected $module;

	/**
	 * @var string
	 */
	protected $component;

	/**
	 * @var string $dependencies
	 */
	protected $dependencies;

	/**
	 * @var string $app
	 */
	protected $app;

	/**
	 * @var string $annotations
	 */
	protected $annotations;

	/**
	 * @var string $composite
	 */
	protected $composite = false;

	/**
	 * @var string $layout
	 */
	protected $layout;

	/**
	 * TemplateUrl is used by controllers or other
	 * If a layout is set, it overrides the templateUrl
	 * @var string
	 */
	protected $templateUrl;

	/**
	 * Routes extracted from source.
	 * Multiple routes can point to the same controller and
	 * @var array
	 */
	protected $routes = array();

	/**
	 * @var string $content
	 */
	protected $content;

	/**
	 * @var string $visible configs
	 */
	protected $visible = array(
		'app',
		'component',
		'module',
		'dependencies',
		'content',
		'routes',
		'templateUrl',
		'composite',
		'state',
		'shortname',
		'title'
	);

	/**
	 * @var array $component aliases
	 */
	protected $componentAliases = array(
		'provider',
		'value',
		'constant',
		'config',
		'run',
		'service',
		'controller',
		'factory',
		'constant',
		'filter',
		'directive',
		'page' => 'controller',
		'dialog' => 'controller',
		'model' => 'factory',
		'api' => 'factory',
		'repositry' => 'factory', // backward compatibility
		'resource' => 'factory',
		'map' => 'constant'
	);

	/**
	 * @var string $defaultComponent
	 */
	protected $defaultComponent = 'provider';

	/**
	 * @var array config
	 */
	protected $config = array();

	/**
	 * @var array Required configs
	 */
	//protected $required = array('src_path', 'pub_path', 'app_path');

	/**
	 * @var array special components
	 */
	protected $specialComponents = array(
		'hasView'	=> array('page', 'dialog'),
		'nameless'	=> array('run', 'config'),
		'valueOnly'	=> array('constant', 'value'),
		'camelCased'=> array('directive')
	);


	public function __construct(Services $services, array $config = null)
	{
		$this->service = $services;
		if($config) $this->configure($config);

		return $this;
	}

	/**
	 * Get annotation
	 * @param string $annotation
	 * @return void
	 */
	protected function getAnnotation($annotation)
	{
		return is_array($this->annotations) && array_key_exists($annotation, $this->annotations)
				? $this->annotations[$annotation]
				: array();
	}

	/**
	 * Set Composite Extension
	 * @return void
	 */
	protected function resolveConfigs()
	{
		$this->config =
			array_merge(
				$this->config,
				$this->service->file->info($this->config('src_path'))
			);

		foreach($this->compositeExtensions as $extension)
		{
			if(false != ($cut = strripos($this->config('basename'), '.'.$extension))) {
				$this->config['filename']  = substr($this->config('basename'), 0, $cut);
				$this->composite = $extension;
			}
		}

		if(!$this->service->string->startsWith(
				$this->config('app_path'),
				$this->config('pub_path')
			)
		) {
			$this->config('app_path',
				$this->service->array->toPath(array(
					$this->config('pub_path'),
					$this->config('app_path')
				))
			);
		}

		$this->config('url_path',
			$this->service->array->toPath(array(
				$this->service->string->difference(
					$this->config('app_path'),
					$this->config('pub_path')
				)
			))
		);

		if(!$this->service->string->startsWith(
				$this->config('src_path'),
				$this->config('app_path')
			)
		) {
			$this->config('src_path',
				$this->service->array->toPath(array(
					$this->config('app_path'),
					$this->config('src_path'))
				)
			);
		}

		if(!($this->config['alias']	= $this->service->file->info($this->config('filename'), 'extension'))) {
			$this->config['filename'].= '.' . ($this->config['alias'] = $this->defaultComponent);
		}

		/* Order matters */
		$this->setContent();
		$this->setAnnotations();
		$this->setComponent();
		$this->setModule();
		$this->setApp();
		$this->setTitle();
		$this->setShortname();
		$this->setRoutes();
		$this->setDependencies();
		$this->setTemplate();
	}

	/**
	 * Set template
	 * @param string $template
	 * @return void
	 */
	public function setTemplate($template = null)
	{
		// is template

		if(!$template)
		{
			// is layout set
			if(($annotation = $this->getAnnotation('layout'))) {
				$template = $this->service->array->toPath(array(
					$this->config('url_path'),
					'layouts',
					array_shift($annotation) . '.html'
				));
			}

			// is templateUrl set
			else if(($annotation = $this->getAnnotation('templateUrl'))) {
				$template = array_shift($annotation);
			}

			// use generic template
			else {
				$template = $this->service->array->toPath(array(
					$this->service->string->difference($this->config('dirname'), $this->config('pub_path')),
					$this->config('filename') . '.html'
				));
			}
		}
	
		$this->templateUrl = (string)$template;
	}

	/**
	 * Set template
	 * @param string $template
	 * @return void
	 */
	public function setDependencies(array $dependencies = array())
	{
		if(empty($dependencies))
		{
			$dependencies = $this->getAnnotation('inject');
		}
		$this->dependencies = $this->service->dependency->build($dependencies);
	}

	/**
	 * Set module
	 * @param string $module
	 * @return void
	 */
	public function setModule($module = null)
	{
		$module = $module?:$this->config('filename');

		$names = $this->getAnnotation('name');

		if(($name = array_shift($names))) $module = $name;

		$this->module =
			in_array($this->component, $this->specialComponents('camelCased'))
			? $this->service->string->camelCase($module)
			: $this->service->string->PascalCase($module);

		$this->_moduleParts = $parts = explode(' ', $this->service->string->split($this->module));
	}

	/**
	 * Set title
	 * @param string $title
	 * @return void
	 */
	public function setShortname($shortname = null)
	{
		if(!($this->shortname = $shortname))
		{
			if(($shortname = $this->config('shortname'))) {
				$this->shortname = $shortname;
			}
			else {
				$parts = $this->_moduleParts;
				array_pop($parts);
				$this->shortname = strtolower(implode(' ', $parts));
			}
		}
	}

	/**
	 * Set title
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title = null)
	{
		if(!($this->title = $title))
		{
			if(($title = $this->config('title'))) {
				$this->title = $title;
			}
			else {
				$parts = $this->_moduleParts;
				array_pop($parts);
				$this->title = ucwords(implode(' ', $parts));
			}
		}
	}

	/**
	 * Set component
	 * @param string $component
	 * @return void
	 */
	public function setComponent($component = null)
	{
		$this->component = $this->convertAlias((string)($component?:$this->config('alias')));
	}

	/**
	 * Set component aliases
	 * @param array $component
	 * @return void
	 */
	public function setComponentAliases(array $aliases)
	{
		foreach((array)$aliases as $key => $val) {
			$this->componentAliases[$key] = $val;
		}
	}

	/**
	 * Set annotations
	 * @param string $annotations
	 * @return void
	 */
	public function setAnnotations($annotations = null)
	{
		$this->annotations = (array)($annotations?:$this->service->annotation->extractAnnotations($this->content));
	}

	/**
	 * Set app name
	 * @param string $name
	 * @return void
	 */
	public function setApp($name = null)
	{
		if(!($this->app = $name))
		{
			if(($name = $this->config('app_name'))) {
				$this->app = $name;
			}
			else if (($name = $this->service->file->info($this->config('app_path'), 'filename'))) {
				$this->app = $name;
			}
		}
	}

	/**
	 * Set Routes
	 * @param array $routes
	 * @return void
	 */
	public function setRoutes(array $routes = null)
	{
		if(empty($routes)) {
			$routes = $this->getAnnotation('route');
		}
		$this->routes = array_values($this->service->array->apply($routes, function($val) {
			return '/' . trim($val, '/');
		}));
	}

	/**
	 * Set content
	 * @param string $contnet
	 * @return void
	 */
	public function setContent($content = null)
	{
		if(!($this->content = $content) && file_exists($this->config('src_path')))
		{
			if($this->config('extension') === 'php')
			{
				ob_start();
				require $this->config('src_path');
				$this->content = ob_get_clean();
			}
			else $this->content = file_get_contents($this->config('src_path'));
		}
	}

	/**
	 * Convert component alias
	 * @param string $component
	 * @return string
	 */
	protected function convertAlias($component)
	{
		$component = $component && array_key_exists($component, $this->componentAliases)? $this->componentAliases[$component]: $component;
		return in_array($component, $this->componentAliases)? $component: $this->defaultComponent;
	}

	/**
	 * Get special components
	 * @param string $type
	 * @return string
	 */
	protected function specialComponents($type)
	{
		return array_key_exists($type, $this->specialComponents)? $this->specialComponents[$type]: array();
	}

	/**
	 * Get special components
	 * @param string $type
	 * @return string
	 */
	public function __get($property)
	{
		if(array_key_exists($property, $this->config)) {
			return $this->config[$property];
		} 

		else if(property_exists($this, $property)) {
			return $this->{$property};
		}
	}

	/**
	 * Output
	 * @return string
	 */
	public function output()
	{
		return $this->service->syntax->namespaced($this);
	}

	/**
	 * Output with tags
	 * @return string
	 */
	public function outputWithTags()
	{
		return '<script>' . $this->output() . '</script>';
	}
}