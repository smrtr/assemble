<?php

namespace Smrtr\Assemble;

use Smrtr\Assemble\Container as Services;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble
 * @supackage Templates
 */
class Templates
{
	/**
	 * @var \Smrtr\Assemble\Container
	 */
	protected $services;

	/**
	 * Directories
	 *
	 * @param array
	 */
	protected $directories = array();

	/**
	 * Files
	 *
	 * @param array
	 */
	protected $files = array();

	/**
	 * @var array $ignore
	 */
	protected $allowedExt = array('html', 'php');

	public function __construct(Services $services, array $directories = null)
	{
		$this->services = $services;

		if($directories) {
			$this->setDirectories($directories);
		}
	}


    /**
	 * Get module
	 *
	 * @param string $module
	 */
	public function setDirectories($directories = array())
	{
		$this->files = array();
		foreach($directories as $directory)
		{
			$this->files = array_merge($this->files, $this->services->file->getFiles($directory, function($info) {
				return (in_array($info['extension'], $this->allowedExt) && strtolower(substr($info['filename'], -4, 4)) === '.tpl');
			}));
		}
		return $this;
	}

	/**
	 * Get routables
	 *
	 * @return array
	 */
	public function getTemplate($content, $name = null)
	{
		if(file_exists($content))
		{
			$name    = $this->services->string->camelCase($this->services->file->info($content, 'filename'));
			$content = file_get_contents($content);
		}

		if($content && $name) 
		{
			return  '<script id="' . $name . '" type="text/ng-template">' . preg_replace('/[\s\t]+/', ' ', $content) . '</script>';
		}

		return null;
	}

	/**
	 * Output
	 *
	 * @return string
	 */
	public function output()
	{
		$contents = '';
		foreach($this->files as $file)
		{
			$contents.= $this->getTemplate($file);
		}
		return $contents;
	}

}
