<?php

namespace Smrtr\Assemble;

use Smrtr\Assemble\Dependency;

/**
 * @author Mwayi Dzanjalimodzi
 * @package Smrtr\Assemble\Component
 * @subpackage Component
 */

class Annotation
{
	/**
	 * @var array tokens
	 */
	protected $tokens = array('inject', 'route', 'templateUrl', 'layout', 'name', 'state', 'shortname', 'title');

	/**
	 * @var array dependency
	 */
	protected $dependency;

	/**
	 * Extract Annotations
	 * @param string $str
	 * @return array matches
	 */
	public function __construct(Dependency $dependency)
	{
		$this->dependency = $dependency;
	}

	/**
	 * Extract Annotations
	 * @param string $str
	 * @return array matches
	 */
	public function extractAnnotations($str)
	{
		$expressions = explode('@', $this->extractComments($str));
		$annotations = array();
		foreach($expressions as $expression)
		{
			if(($expr = $this->resolveTokenValuePairExpression($expression)) && in_array($expr->token, $this->tokens) && $expr->value)
			{
				$values = array_unique(array_filter(explode(',', $expr->value)));

				foreach($values as $value)
				{
					if(!array_key_exists($expr->token, $annotations)) {
						$annotations[$expr->token] = array();
					}
					$value = $this->dependency->resolveAnnotationParameter(0, $value);
					$annotations[$expr->token][$value->annotation] = $value->parameter;
				}
			}
		}
		return $annotations;
	}

	/**
	 * Insert Instruction Separation
	 * @param string $code
	 * @return string
	 */
	protected function insertInstructionSeparartion($code)
	{
		return preg_replace('/(\s+)?[\n]+/', ";\n", $code);
	}

	/**
	 * Extract Comment
	 * @param string $str
	 * @return mixed
	 */
	protected function extractComments($code)
	{
		preg_match_all("/(\/\*).*?(\*\/).*?(\n|\$)/s", $this->insertInstructionSeparartion($code), $matches);
		return preg_replace('/[\s\t\r\n]+/', ' ', implode("\n", array_shift($matches)));
	}

	/**
	 * Resolve Token Value Pair Expression
	 * @param string $str
	 * @return mixed
	 */
	protected function resolveTokenValuePairExpression($str)
	{
		// trim comment tags
		$str   = preg_replace('/(\/\*)|(\*\/)/', '', trim($str));

		// extract token:values
		$pos   = strpos($str, ':');
		$token = trim(substr($str, 0, $pos));
		$value = trim(substr($str, $pos + 1, strlen($str)));

		$value = strpos($value, ';')
				 ? trim(preg_replace('/\;.*$/', "", $value), '; ')
				 : trim(preg_replace('/\n.*$/', "", $value), '* ');

		return $value && $token? (object)array('token' => $token, 'value' => $value): false;
	}

}