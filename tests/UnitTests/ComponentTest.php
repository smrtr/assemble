<?php

use Smrtr\Assemble\Container;
use Smrtr\Assemble\Component;

class ComponentTest extends PHPUnit_Framework_TestCase
{
    /**
	 * @var \Smrtr\Assemble\Container $service
	 */
	protected $service;

	/**
	 * @var \Smrtr\Assemble\Component example physical component
	 */
	protected $physicalComponent;

	protected function setUp()
	{
		$this->service = new Container;

		$this->physicalComponent = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/pages/manageResource.page.js'
		));
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testCompositeComponentExtensionFromFileName()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/configs/main.value.js.php'
		));

		$properties = $component->getVisibleProperties();

		$this->assertEquals('MainValue', $properties['module']);
		$this->assertEquals('js.php', $properties['composite']);
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testPhpOutputFile()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/configs/main.value.js.php'
		));

		$this->assertEquals('["Test","Test","Test","Test"]', $component->content);
	}


	/**
     * @covers \Smrtr\Assemble\Component
	 * @todo more testst to be done here
     */
	public function testDependenciesFromRealFile()
    {
		$properties = $this->physicalComponent->getVisibleProperties();

		$this->assertInstanceOf('Smrtr\Assemble\Dependency', $properties['dependencies']);

		$this->assertContains('$scope', $properties['dependencies']->injectables());
		$this->assertContains('RMS', $properties['dependencies']->injectables());
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testBadFileNameStrategy()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/badly/named/assemble/script/main.js'
		));

		$properties = $component->getVisibleProperties();

		$this->assertEquals('MainProvider', $properties['module']);
		$this->assertEquals('provider', $properties['component']);
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testSetDirectiveModuleNameFromFileName()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/directives/main.directive.js'
		));

		$properties = $component->getVisibleProperties();
		$this->assertEquals('mainDirective', $properties['module']);
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testSetModuleNameFromFileName()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/controllers/main.controller.js'
		));

		$properties = $component->getVisibleProperties();
		$this->assertEquals('MainController', $properties['module']);
	}


	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testComponentAliasFromFileName()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/test',
			'src_path' => '/pages/main.page.js'
		));

		$properties = $component->getVisibleProperties();

		$this->assertEquals('MainPage', $properties['module']);
		$this->assertEquals('controller', $properties['component']);
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testSetAppFromFileName()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/app',
			'src_path' => '/modules/bridge/configs/config.page.js'
		));

		$properties = $component->getVisibleProperties();

		$this->assertEquals('app', $properties['app']);
	}

	/**
     * @covers \Smrtr\Assemble\Component
     */
	public function testSetAppFromConfig()
    {
		$component = new Component($this->service, array(
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www',
			'app_path' => '/apps/app',
			'src_path' => '/modules/bridge/configs/config.page.js',
			'app_name' => 'test'
		));

		$properties = $component->getVisibleProperties();

		$this->assertEquals('test', $properties['app']);
	}
}