<?php

use Smrtr\Assemble\Container;

class ContainerTest extends PHPUnit_Framework_TestCase
{
	/**
     * @var \Smrtr\Assemble\Container
     */
	protected $container;

	protected function setUp()
	{
		$this->container = new Container;
	}

    /**
     * @covers \Smrtr\Assemble\Container
     */
    public function testContainerHasBaseDependencies()
    {
		$dependencies = array(
			'annotation'	=> 'Smrtr\Assemble\Annotation',
			'array'			=> 'Smrtr\Assemble\Utility\Arr',
			'string'		=> 'Smrtr\Assemble\Utility\String',
			'dependency'	=> 'Smrtr\Assemble\Dependency',
			'syntax'		=> 'Smrtr\Assemble\Syntax',
		);

		foreach($dependencies as $name => $class) {
			$this->assertInstanceOf($class, $this->container->{$name});
		}
    }

}