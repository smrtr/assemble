<?php

use Smrtr\Assemble\Annotation;
use Smrtr\Assemble\Dependency;
use Smrtr\Assemble\Utility\Arr;

class AnnotationTest extends PHPUnit_Framework_TestCase
{
    /**
	 * @var \Smrtr\Assemble\Annotation
	 */
	protected $annotation;

	/**
	 * @var code sample
	 */
	protected $codeSample = '
		/**
		 * @inject: Dependency1 as d1, Dependency2, Dependency3 as mwayi
		 * @inject: Dependency4; ignore anything after the instruction separation ";"
		 * @route: /about/team, /about/company, /profile/:id
		 * @templateUrl: /path/to/template.html
		 * @invalidToken: this token is invalid;
		 */
		 var numbers = [1,2,3,4,5];

		 for(i in numbers) {
			console.log(i, numbers[i]);
		 }

		 var string = "@name: Do not match valid token outside of comment block;"

		 // @inject: Dependency5, Dependency4, Dependency10
		 // @inject: Dependency7
		 // @inject: Dependency6';

	protected function setUp()
	{
		$this->annotation = new Annotation(new Dependency(new Arr));
	}

    /**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testExtractAnnotationsFromCodeToken()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);
		$this->assertArrayHasKey('inject', $annotations);
		$this->assertArrayHasKey('route', $annotations);
		$this->assertArrayHasKey('templateUrl', $annotations);
    }

	/**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testDoNotMatchTokenOutsideOfCommentBlock()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);
		$this->assertArrayNotHasKey('name', $annotations);
    }

	/**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testDoNotMatchInvalidToken()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);
		$this->assertArrayNotHasKey('invalidToken', $annotations);
    }

	/**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testAnnotationsHaveAccurateParametersToken()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);

		// Multiline annotation
		$this->assertInternalType('array', $annotations['inject']);
		$this->assertCount(8, $annotations['inject']);

		$this->assertArrayHasKey('Dependency1', $annotations['inject']);
		$this->assertArrayHasKey('Dependency2', $annotations['inject']);
		$this->assertArrayHasKey('Dependency3', $annotations['inject']);
		$this->assertArrayHasKey('Dependency4', $annotations['inject']);
		$this->assertArrayHasKey('Dependency5', $annotations['inject']);
		$this->assertArrayHasKey('Dependency6', $annotations['inject']);
		$this->assertArrayHasKey('Dependency7', $annotations['inject']);
		$this->assertArrayHasKey('Dependency10', $annotations['inject']);

		// Single line annotation
		$this->assertInternalType('array', $annotations['templateUrl']);
		$this->assertContains('/path/to/template.html', $annotations['templateUrl']);
    }

	/**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testAsParameterStatement()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);
		$this->assertEquals('d1', $annotations['inject']['Dependency1']);
    }

	/**
     * @covers \Smrtr\Assemble\Annotation::extractAnnotations
     */
    public function testInstructionSeparartion()
    {
		$annotations = $this->annotation->extractAnnotations($this->codeSample);
		$this->assertNotContains('ignore anything after the instruction separation ";"', $annotations['inject']['Dependency4']);
    }
}