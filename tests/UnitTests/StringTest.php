<?php

use Smrtr\Assemble\Utility\String;

class StringTest extends PHPUnit_Framework_TestCase
{
    /**
	 * @var \Smrtr\Assemble\String
	 */
	protected $string;

	protected $testStrings = array(
		'Convert this   sentence.',
		'Convert-this---sentence.',
		'convert.this...sentence.',
		'convert_this___sentence.',
		'ConvertThisSentence.',
		'convertThisSentence.',
		'convert-this___sentence.', // mix
	);

	protected function setUp()
	{
		$this->string = new String;
	}

    /**
     * @covers \Smrtr\Assemble\Utility\String::pascalCase
     */
    public function testPascalCase()
    {
		foreach($this->testStrings as $testString) {
			$actual = $this->string->pascalCase($testString);
			$this->assertEquals('ConvertThisSentence', $actual);
		}
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::camelCase
     */
    public function testCamelCase()
    {
		foreach($this->testStrings as $testString) {
			$actual = $this->string->camelCase($testString);
			$this->assertEquals('convertThisSentence', $actual);
		}
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::title
     */
    public function testTitle()
    {
		foreach($this->testStrings as $testString) {
			$actual = $this->string->title($testString);
			$this->assertEquals('Convert This Sentence', $actual);
		}
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::underscore
     */
    public function testUnderscore()
    {
		foreach($this->testStrings as $testString) {
			$actual = $this->string->underscore($testString);
			$this->assertEquals('convert_this_sentence', $actual);
		}
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::snakeCase
     */
    public function testSnakeCase()
    {
		foreach($this->testStrings as $testString) {
			$actual = $this->string->snakeCase($testString);
			$this->assertEquals('convert-this-sentence', $actual);
		}
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::difference
     */
    public function testStringDifference()
    {

		$actual = $this->string->difference('assemble/tests/www/', 'tests/www/');
		$this->assertEquals('assemble/', $actual);

		$actual = $this->string->difference('assemble/tests/www/', 'assemble/');
		$this->assertEquals('tests/www/', $actual);

		$actual = $this->string->difference('assemble/tests/www/', 'assem');
		$this->assertEquals('ble/tests/www/', $actual);

		$actual = $this->string->difference('assemble/tests/www/', 'emble/');
		$this->assertNotEquals('tests/www/', $actual);
    }

	/**
     * @covers \Smrtr\Assemble\Utility\String::snakeCase
     */
    public function testStartsWith()
    {

		$actual = $this->string->startsWith('assemble/tests/www/', 'assemb');
		$this->assertTrue($actual);

    }

}