<?php

use Smrtr\Assemble\Syntax;
use Smrtr\Assemble\Component;
use Smrtr\Assemble\Container;
use Smrtr\Assemble\Dependency;
use Smrtr\Assemble\Utility\Arr;

class SyntaxTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @var \Smrtr\Assemble\Syntax
	 */
	protected $syntax;

	/**
	 * @var \Smrtr\Assemble\Component
	 */
	protected $component;

	protected function setUp()
	{
		$this->syntax = new Syntax(new Container);
		$this->component = new Component(new Container, array(
			'src_path' => '/pages/manageResource.page.js',
			'app_name' => 'app', // override app name
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www/',
			'app_path' => 'apps/test', //can calculate app name from foler name,
		));
	}

    /**
     * @covers \Smrtr\Assemble\Syntax::constructor
     */
    public function testConstructorWithModules()
    {
		$actual = $this->syntax->constructor('app', array('example as ui.example', 'ui.bootstrap', 'ngRoute'));
		$this->assertEquals("angular.module('app',['ui.example','ui.bootstrap','ngRoute'])", $actual);
    }

	/**
     * @covers \Smrtr\Assemble\Syntax::constructor
     */
    public function testConstructorWithoutModules()
    {
		$actual = $this->syntax->constructor('app', array());
		$this->assertEquals("angular.module('app',[])", $actual);
    }


	/**
     * @covers \Smrtr\Assemble\Syntax::constructor
     */
    public function testNamespaced()
    {
		$component = new Component(new Container, array(
			'src_path' => '/components/controller.controller.js',
			'app_name' => 'app', // override app name
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www/',
			'app_path' => 'apps/test', //can calculate app name from foler name,
		));
		echo $actual = $this->syntax->namespaced($component);
    }
}