<?php

use Smrtr\Assemble\Dependency;
use Smrtr\Assemble\Utility\Arr;

class DependencyTest extends PHPUnit_Framework_TestCase
{
    /**
	 * @var \Smrtr\Assemble\Dependency
	 */
	protected $depenency;


	/**
	 * @var array
	 */
	protected $input = array('Dep1', 'Dep10 as Dep3', 'Dep45' => 'Dep5', 'Dep2 AS Dep6');

	protected function setUp()
	{

	}

    /**
     * @covers \Smrtr\Assemble\Dependency
     */
    public function testBasics()
    {
		$dependency = new Dependency(new Arr, $this->input);

		$this->assertArrayHasKey('Dep1', $dependency->parameters());
		$this->assertArrayHasKey('Dep2', $dependency->parameters());
		$this->assertArrayHasKey('Dep45', $dependency->parameters());

    }
}