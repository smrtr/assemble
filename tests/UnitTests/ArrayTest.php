<?php

use Smrtr\Assemble\Utility\Arr;

class ArrayTest extends PHPUnit_Framework_TestCase
{
    /**
	 * @var \Smrtr\Assemble\Array
	 */
	protected $array;

	protected $testArr = array('Think/', '/Smart/.', 'Work', '"Smrtr"');

	protected function setUp()
	{
		$this->array = new Arr;
	}

    /**
     * @covers \Smrtr\Assemble\Utility\Arr::Wrap
     */
    public function testWrap()
    {
		$expected = array('"Think/"', '"/Smart/."', '"Work"', '""Smrtr""');

		$actual = $this->array->wrap($this->testArr, '"');

		$this->assertEquals($expected, $actual);
    }

	/**
     * @covers \Smrtr\Assemble\Utility\Arr::Trim
     */
    public function testTrim()
    {
		$expected = array('Think', 'Smart', 'Work', 'Smrtr');

		$actual = $this->array->trim($this->testArr, '"./');

		$this->assertEquals($expected, $actual);
    }

	/**
     * @covers \Smrtr\Assemble\Utility\Arr::Join
     */
    public function testJoin()
    {
		$array = array('Think', 'Smart', 'Work', 'Smrtr');
		$expected = 'Think. Smart. Work. Smrtr';

		$actual = $this->array->join($array, '. ');

		$this->assertEquals($expected, $actual);
    }

	/**
     * @covers \Smrtr\Assemble\Utility\Arr::toPath
     */
    public function testToPath()
    {
		$array = array('/Think', 'Smart/', 'Work', 'Smrtr');
		$expected = '/Think/Smart/Work/Smrtr';

		$actual = $this->array->toPath($array);

		$this->assertEquals($expected, $actual);
    }
}