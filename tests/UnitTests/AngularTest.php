<?php

use Smrtr\Assemble\Angular;

class AngularTest extends PHPUnit_Framework_TestCase
{
	protected $angular;

	/*
	 * @var string Public Directory
	 */
	protected $config;

	protected function setUp()
	{
		$this->config = array(
			'app_name' => 'app', // override app name
			'pub_path' => dirname(dirname(__DIR__)). '/tests/www/',
			'app_path' => dirname(dirname(__DIR__)). '/tests/www/apps/test', //can calculate app name from foler name,
		);

		$angular = new Angular($this->config);
		//echo $angular->output();exit;
	}

	/**
     * @covers Smrtr\Assemble\Angular::output
	 * @covers Smrtr\Assemble\Angular::packages
	 * @covers Smrtr\Assemble\Syntax::constructor
     */
    public function testUrlPath()
    {
		$angular = new Angular($this->config);
		$this->assertEquals('/apps/test', $angular->config('url_path'));
    }

    /**
     * @covers Smrtr\Assemble\Angular::output
	 * @covers Smrtr\Assemble\Syntax::constructor
     */
    public function testAngularAppConstructorWithoutDependencies()
    {
		$angular = new Angular($this->config);
		$this->assertContains("angular.module('app',[]);", $angular->output());
    }

	/**
     * @covers Smrtr\Assemble\Angular::output
	 * @covers Smrtr\Assemble\Angular::packages
	 * @covers Smrtr\Assemble\Syntax::constructor
     */
    public function testAngularAppConstructorWithDependencies()
    {
		$angular = new Angular($this->config);
		$angular->packages(array('ui.bootstrap'));
		$this->assertContains("angular.module('app',['ui.bootstrap']);", $angular->output());
    }

}