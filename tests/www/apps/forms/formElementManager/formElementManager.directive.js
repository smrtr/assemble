/* @inject: $log, $compile, StringService, inputTypes, $templateCache*/

var directive = {
	scope: {
		input: '=formElementManagerDirective',
	},
	controller: 'FormElementManagerController',
	restrict: 'A',
	link: function(scope, iElem, iAttr) {

		if(angular.isDefined(scope.input)) {
			scope.setOptions(scope.input.item.options);
			var content = iElem.html( $templateCache.get('formElementManagerDirectiveTpl') ).contents();
			$compile(content)(scope);
		}
		else $log.error('Input Directive must be defined with object settings');
	}
}

return directive;