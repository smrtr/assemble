/*@inject: $timeout, $scope, CollectionFactory, inputTypes, select2Services, DomService, StringService, $rootScope*/


$scope.required = true;
$scope.types = inputTypes;
$scope.select2Services = select2Services;

$scope.select2Suggest = DomService.select2ApiServiceConfig;

$scope.setOptions = function(options) {
	$scope.options = new CollectionFactory(options);
	$scope.input.item.options = $scope.options.get();
}


$scope.disallowOption = function(index, label) {

	$timeout(function(){
		if($scope.options.countColumnItems('label', label) > 1) {
				$scope.options.removeById(index)
		}
	},2000);
}

if(!angular.isDefined($scope.input.item.widget_open)) {
	$scope.input.item.widget_open = true;
}


if(!angular.isDefined($scope.input.item.widget_advanced_open)) {
	$scope.input.item.widget_advanced_open = false;
}


$scope.columns = [
	{w: 1, n:12},
	{w: 2, n:6},
	{w: 3, n:4},
	{w: 4, n:3},
	{w: 6, n:2},
	{w: 12, n:1},
];


$scope.$watch('input.item.question', function(nv, ov) {
	$scope.input.item.name = StringService.toSlug(nv, '_');
});