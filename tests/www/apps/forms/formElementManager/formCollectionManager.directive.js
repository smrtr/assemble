/* @inject: $log, $compile, StringService, $templateCache */

var str = StringService;

var directive = {
	scope: { items: '=formCollectionManagerDirective' },
	controller: 'FormCollectionManagerController',
	restrict: 'A',
	link: function(scope, iElem, iAttr) {

		var el = angular.element($templateCache.get('formCollectionManagerDirectiveTpl'));

		scope.dropzone = iAttr.formElementStencilDropzone;

		var list = el.find('ul');
		list.sortable({
			handle: ".form-element-handle",
			cursor: "move",
			scroll: true,
			delay: 0,
			revert: 0,
			placeholder: "sortable-placeholder"
		});

		//list.disableSelection();

		list.on( "sortdeactivate", function( e, ui ) {
			var item = angular.element(ui.item);
			var from = item.scope().$index;

			var to = list.children().index(ui.item);

			if(to >= 0){
				scope.$apply(function(){
					if(from >= 0){
						scope.$emit('sortable:sorted', {from:from,to:to});
					}else{
						scope.$emit('sortable:created', {to:to, item:item});
						ui.item.remove();
					}
				})
			}
		});

		$compile(iElem.append(el.contents()).contents())(scope);
	}
}



return directive;