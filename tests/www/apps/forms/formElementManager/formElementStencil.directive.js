/* @inject:  $timeout, $log, ErrorService */

return {
	scope: {
		type:'=formElementStencilDirective'
	},
	link:function(scope, el, attrs) {

		ErrorService.attempt(function() {
			return angular.isDefined(scope.type.type);
		}, 'Element type not defined');

		ErrorService.attempt(function() {
			return angular.isDefined(scope.type.dropzone);
		}, 'Dropsoze not supplied');

		if(scope.type.active) {
			el.attr('form-element-stencil-directive', angular.toJson(scope.type));
			var dropzone = '#' + scope.type.dropzone;
			el.draggable({
				connectToSortable: dropzone,
				helper: "clone",
				revert: "invalid"
			});
			el.disableSelection();
		}
	}
}