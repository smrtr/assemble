/* @inject: $scope, CollectionFactory*/

$scope.setItems = function(items) {
	$scope.items = new CollectionFactory(items);
}

$scope.$on('sortable:sorted', function(ev, pos) {
	$scope.items.move(pos.from, pos.to);
})

$scope.$on('sortable:created',function(ev, meta){
	var item = $scope.$eval(meta.item.attr('form-element-stencil-directive'));
	$scope.items.insert(item, meta.to - 1);
});

