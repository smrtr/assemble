/*
 * @inject: $scope, $location,StringService, inputTypes, systemTokens, CollectionFactory
 */
$scope.types = inputTypes;

$scope.params.subform = $scope.params.subform || [];

var origin;
$scope.confirm = function () {
	$scope.params.subform = $scope.params.subform.serialise();
	$scope.$close(true);
};


$scope.close = function () {
	$scope.params.subform = $scope.params.subform.serialise();
	$scope.$close(false);
};

$scope.types = inputTypes;
$scope.tokens = systemTokens;

$scope.configs = {dropzone: 'subform'}
console.log('$scope.params.item.subform', $scope.params.subform);


// quick fix
if(angular.isArray($scope.params.subform ) || angular.isObject($scope.params.subform )) {

	$scope.params.subform = new CollectionFactory($scope.params.subform);
}
else $scope.params.subform = new CollectionFactory([]);

