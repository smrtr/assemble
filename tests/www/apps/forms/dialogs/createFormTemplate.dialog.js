/*
 * @inject: $scope, FormTemplateModel, $location
 */
$scope.params = {
	name: null,
	description: null
}
$scope.confirm = function () {
	var params = {};
	if($scope.params.name) { 
		params.name = $scope.params.name;

		if($scope.params.description) {
			params.description = $scope.params.description;
		}
		FormTemplateModel.create(params).then(function(data) {
			if(data.response) {
				$location.path('templates/' + data.response);
				$scope.$close(true);
			}
		});
	}
};

$scope.close = function () {
	$scope.$close(true);
};

