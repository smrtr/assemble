/*
 * @inject: $scope, FormModel, FormTemplateModel, $location, $rootScope
 */

$scope.confirm = function () {

	if($scope.params.template.id) {
		var params = {
			template_id: $scope.params.template.id,
			entity_id: $scope.params.entity.id,
		};
		FormModel.create(params).then(function(data) {
			if(data.response) {

				var privs = null;
				if((privs = $rootScope.currentUser.get('privs'))){
					if(privs.FormsRaiser) {
						$rootScope.$broadcast('notify:error:globalStatic', {type:'success', autoHide:true, message: 'The form "'+ $scope.params.template.name +'" has been raised.'});
						return $scope.$close(true);
					}
				}
				$location.path('form/' + data.response);
				$scope.$close(true);
			}
		});
	}
};


$scope.close = function () {
	$scope.$close(true);
};

