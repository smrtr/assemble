/*@inject: $compile, $rootScope, $window, $timeout, $templateCache, StringService*/
return {

		scope: {
			settings: '=repeatableRegionDirective'
		},
    restrict: 'A',
		priority: 1,
	  controller: function($scope, BaseModel){
			delete $scope.settings.blueprint.$$hashKey;
			delete $scope.settings.blueprint.model;
			$scope.items = [];
			$scope.fetchData = function() {
				var model = new BaseModel('asset');
				return model.base('sub_assets', {id: 4969, select: 'enum'}).then(function(data){
					if(data.response) {
						$scope.$emit('cloak-directive:success:random');
						var rows = data.response.rows;
						for(var i in rows) {
							var blueprint = [];
							angular.copy($scope.blueprint, blueprint);

							$scope.items.push(angular.extend({item: rows[i]}, {blueprint: blueprint}));
						}

					}else{
						$scope.$emit('cloak-directive:error:random', {
							template: '<div class="alert alert-danger">Could not find repeatable data</div>'
						});
					}

				});
			}
		},
		require: 'ngModel',
    link: function (scope, el, attrs, ngModel) {

			scope.blueprint = StringService.serialise(angular.toJson(scope.settings.blueprint));
			var content = el.html( $templateCache.get('repeatableRegionDirectiveTpl') ).contents();
			$compile(content)(scope);

			scope.fetchData();
    }
};

