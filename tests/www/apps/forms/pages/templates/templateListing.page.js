/* @inject: $scope, CollectionFactory, FormTemplateService, FormModel, $timeout, PollService
 * @route: /templates
 */

$scope.forms = [];


FormTemplateService.get({}).then(function(collection) {
	if(collection) {
		$scope.$emit('cloak-directive:success:forms');
		$scope.forms = collection;
	}else {
		$scope.$emit('cloak-directive:error:forms');
	}
});