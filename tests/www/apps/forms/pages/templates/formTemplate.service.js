/*@inject: FormTemplateModel, CollectionFactory, $location, $rootScope, StringService, DomService, $timeout, ErrorService*/
var str = StringService, service = {};

service.setCurrentId  = function(id) {
	service.id = id;
}

service.save = function(id, params, message) {

	var _params = angular.extend({}, params || {});

	if(angular.isObject(params) && typeof params.json_schema === 'object') {
		_params.json_schema = params.json_schema.serialise();
	}
	return $rootScope.$broadcast('notify:working:global', {
		message: message || 'Working...',
		onComplete: function() {
			return FormTemplateModel.update(id, _params).then(function(data) {
				if(data.response) {
					$rootScope.$broadcast('notify:success:global', {message: 'Saved'});
				}else {
					$rootScope.$broadcast('notify:error:global', {message: 'Unable to save'});
				}
			});
		}
	});
}


var FormElementsCollection = CollectionFactory.extend({
	events: {

		'post:add': function(properties, schema) {
			DomService.scroll('#element' + properties.$id, 'slow').then(function(){
				//schema.exclusiveSelect(properties.$id);
			}).then(function(){
				service.save(service.id, {json_schema: schema}, 'Adding new element...');
			});
		},
		'pre:removeById': function(properties, schema) {
			return $rootScope.dialog('confirm').then(function(response) {
				return response? properties: undefined;
			});
		},
		'post:removeById': function(properties, schema) {
			if(properties) service.save(service.id, {json_schema: schema}, 'Removing element...');
		},
		'post:exclusiveSelect': function(id, schema) {
			if(id) DomService.scroll('#element' + id, 'fast');
		},
	},

	// do not copy over model info
	hydrateBlacklist: ['$id', '$selected', '$$hashKey', 'model'],
	add: function(properties)
	{
		if(angular.isObject(properties))
		{
			properties = this.cleanItem(properties);
			properties.$id = this.autoIncrement++;
			properties.$selected = false;
			properties.conditionalNumberingSkip = false;
			if(['sectionbreak', 'header', 'freetext'].indexOf(properties.type) !== -1) {
				properties.conditionalNumberingSkip = true;
			}

			properties.name = StringService.toSlug(properties.question, '_');
			this.origin.push(properties);
		}

		return properties;
	}
});


service.removeOne = function(id) {
	$rootScope.dialog('confirm').then(function(response){
		if(response) return FormTemplateModel.remove(id);
	});
}

service.createVersion = function(id) {
	return FormTemplateModel.createVersion(id);
}

service.getOne = function(id) {
	return FormTemplateModel.hydrate('row').getOne(id).then(function(data) {
		if(data.response) {
			var jsonSchemaObject = str.serialise(data.response.json_schema);
			data.response.json_schema = new FormElementsCollection(jsonSchemaObject);
			data.response.jsonSchemaObject = jsonSchemaObject;
			data.response.meta = str.serialise(data.response.meta);
		}
		return data.response;
	});
}

service.get = function(filter) {
	return FormTemplateModel.hydrate('rows').get({select:'listing_details'}).then(function(data) {
		return new CollectionFactory(data.response);
	});
}
service.publish = function(id) {
	return $rootScope.dialog('confirm').then(function(response){
		if(response) {
			return FormTemplateModel.update(id, {status: 'published'});
		}
		return false;
	});
}

service.linkedForms = function(id) {
	return FormTemplateModel.linkedForms(id).then(function(data) {
		return data.response;
	});
}

return service;