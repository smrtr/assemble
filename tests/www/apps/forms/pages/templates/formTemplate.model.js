/*@inject: BaseModel*/
var model = new BaseModel('form_template');

model.linkedForms = function(id) {
	return model.base('linked_forms', {template_id:id})
}


model.createVersion = function(id) {
	return model.hydrate('inherit').base('create_version', {id:id})
}
return model;