/**
 * @inject: $rootScope, CurrentUser, $scope, CollectionFactory, StringService, inputTypes, FormTemplateService, FormModel, $timeout, PollService, $location, systemTokens
 * @route: /templates/:id
 * @resolve:
 * ({
 *	 	CurrentUser: ['CurrentUserService', function(CurrentUserService) {
 *	 		return CurrentUserService;
 *	 	}]
 * })
 */

$scope.id = $scope.$routeState.params.id;
$scope.tab = 'builder';
$scope.types = inputTypes;
$scope.hasRow = false;
$scope.configs = {
	tab: 'builder',
	formSettingTab: 'display',
	disableInputs: true,
	dropzone: 'main'
}
$scope.tokens = systemTokens;


FormTemplateService.setCurrentId($scope.id);

FormTemplateService.getOne($scope.id).then(function(data) {
	if(data) {
		$scope.$emit('cloak-directive:success:form');
		$scope.form = data;
		$scope.elements = data.json_schema;

		$scope.form.header = $scope.form.header || {}

		$scope.form.json_schema.saveForm = function() {
			$scope.save();
		}

		$scope.form.align = 'top';

		$scope.form.isPublished = function() {
			return $scope.form.status === 'published';
		}

		if($scope.form.isPublished()) {
			$scope.tab = 'preview';
		}else {
			PollService.interval = 1000 * 60 * 5
			PollService.launch(function(){
				$scope.save('Auto saving...');
			});
		}
		if($scope.$routeState.params.existingTail && $scope.form.status == 'draft') {
			$rootScope.$broadcast('notify:error:globalStatic', {
				type: 'warning',
				autoHide: true,
				message: 'This version template needs to be published before it can move to a new version'
			});
		}
	}else {
		$scope.$emit('cloak-directive:error:form', {
			header: '404',
			body: 'Could not find template ' + $scope.id + '. Return to <a href="#/templates">template listiing</a>.'
		});
	}
});

$scope.showSettingsDialog = function(item) {
	return $scope.dialog('subformSettings', item,{blackout:true}).then(function(response){
		if(response) {
			$scope.save();
		}

		return item;

	});
}

$scope.showEditorDialog = function(item) {
	$scope.dialog('richtext', item,{blackout:true}).then(function(response){

		return response;
	}).then(function(response){
		if(response) $scope.save();
	});
}
$scope.removeHeaderFile = function() {
	$scope.form.meta.header = false;
	if(!$scope.form.meta.header) {
		return FormTemplateService.save($scope.id, $scope.form);
	}
}
$scope.save = function(message) {

	// @to add isSaveable() method
	console.log($scope.form);
	if(!$scope.form.isPublished()) {
		console.log('test', $scope.form.meta.header);
		return FormTemplateService.save($scope.id, $scope.form, message);
	}
	$scope.dialog('message', {
		confirm: true,
		header:'You cannot modify a published template',
		body:'<h1>Proceed to create a new version of this template?</h1>'
	}, {blackout:true}).then(function(response){

		if(response) {
			$scope.createVersion();
		}
	});
}

$rootScope.publishForm = $scope.publish = function() {
	return FormTemplateService.publish($scope.id).then(function(response){
		if(response) {
			$scope.form.status = 'published';
			PollService.cancel();
		}

	});
}

$scope.createVersion = function() {
	return FormTemplateService.createVersion($scope.id).then(function(data){
		if(data.response) {
			return $location.path('/templates/' + data.response).search({
				existingTail: false
			});
		}
		else {
			$location.path('/templates/' + $scope.form.current_version_id).search({
				existingTail: true
			});
		}
	});
}

$scope.$watch('configs.tab', function(nv, ov){
	if($scope.form && nv == 'preview') {
		$scope.elements = new CollectionFactory($scope.form.json_schema.get());
	}
});

$scope.$on('$destroy', function(){
	PollService.cancel();
})