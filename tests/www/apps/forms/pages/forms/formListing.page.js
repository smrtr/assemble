/* @inject: $scope, CollectionFactory, FormService, FormModel, $timeout, PollService
 * @route: /
 */

$scope.forms = [];


FormService.get({}).then(function(collection) {
	if(collection) {
		$scope.$emit('cloak-directive:success:forms');
		$scope.forms = collection;
	}else {
		$scope.$emit('cloak-directive:error:forms');
	}
});