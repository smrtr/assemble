/*@inject: BaseModel*/
var model = new BaseModel('forms');

model.embeddedAssociations = function(id){
	return model.base('object_with_associations', {id: id, select: 'verbose'});
}

model.updateData = function(id, data){
	return model.base('update_data', {id: id, data:data});
}
return model;