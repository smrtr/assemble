/*
 *@inject: $scope, FormService, FormTemplateService
 *@route: /form/:id
 **/
$scope.hasRow = true;
$scope.id = $scope.$routeState.params.id;

FormService.getOne($scope.$routeState.params.id).then(function(data) {

	if(data) {
		$scope.$emit('cloak-directive:success:form');
		$scope.form = data.forms;
		$scope.form.align = 'top'
		$scope.elements = data.forms.json_schema;
		$scope.entity = data;
	}else {
		$scope.$emit('cloak-directive:error:form', {
			header: '404',
			body: 'Could not find form ' + $scope.id + '. Return to <a href="#/">form listiing</a>.'
		});
	}
});

$scope.save = function(message) {
	return FormService.save($scope.$routeState.params.id, $scope.form, message);
}

