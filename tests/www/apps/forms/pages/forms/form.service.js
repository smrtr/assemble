/*@inject: FormModel, CollectionFactory, $rootScope, StringService, DomService, $timeout, ErrorService*/
var str = StringService;

var FormElementsCollection = CollectionFactory.extend({
	events: {
		'pre:add': function(properties, schema) {
			properties.conditionalNumberingSkip = false;
			if(['sectionbreak', 'header', 'freetext'].indexOf(properties.type) !== -1) {
				properties.conditionalNumberingSkip = true;
			}
			return properties;
		},

	}
});


/**
 *
 */
this.save = function(id, params, message) {
	var _params = angular.extend({}, params);
	var _this = this;
	if(angular.isObject(params) && params.json_schema instanceof CollectionFactory) {

		return $rootScope.$broadcast('notify:working:global', {
			message: message || 'Working...',
			onComplete: function() {
				var formData = params.json_schema.serialise();


				return FormModel.updateData(id, formData).then(function(data) {
					if(data.response) {
						$rootScope.$broadcast('notify:success:global', {message: 'Saved'});
					}else {
						$rootScope.$broadcast('notify:error:global', {message: 'Unable to save'});
					}
				});
			}
		});
	}
}

this.removeOne = function(id) {
	$rootScope.dialog('confirm').then(function(response){
		if(response) {
			return FormModel.remove(id);
		}
	});
}

this.getOne = function(id) {
	return FormModel.embeddedAssociations(id).then(function(data) {

		if(data.response.forms) {
			if(angular.isDefined(data.response.forms)) {
				var jsonSchemaObject = str.toJson(data.response.forms.json_schema);
				data.response.forms.json_schema = new FormElementsCollection(jsonSchemaObject, true);
				data.response.forms.meta = str.serialise(data.response.forms.meta);

			}
		}
		return data.response;
	});
}

this.get = function(filter) {
	return FormModel.hydrate('rows').get({select:'verbose'}).then(function(data) {
		return new CollectionFactory(data.response);
	});
}