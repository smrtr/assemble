[
	{'service': 'user', 'name': 'Users', 'search_on': 'name'},
	{'service': 'customer', 'name': 'Properties', 'search_on': 'customer'},
	{'service': 'asset', 'name': 'Assets', 'search_on': 'descr'},
	{'service': 'audit', 'name': 'Audits', 'search_on': 'description'},
	{'service': 'form_template', 'name': 'Forms', 'search_on': 'name'},
	{'service': 'job', 'name': 'Jobs', 'search_on': 'description'},
]