{

	'job.description': 'job.description',
	'job.id': 'job.jid',
	'job.type': 'job.type',
	'job.reporter': 'job.rep_by',
	'job.status': 'job.status',
	'customer.name': 'customer.customer',
	'customer.group': 'customer_group.name',
	'customer.reference': 'customer.ref',
	'customer.address1': 'customer.addr_1',
	'customer.address2': 'customer.addr_2',
	'customer.address3': 'customer.addr_3',
	'customer.postcode': 'customer.postcode'
}