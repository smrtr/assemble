{
	job: {
		systemTokens:{
			exclude: []
		},
		repeatableOptions:{
			asset:{
				name: 'Sub Assets',
				model:'asset',
				method:'sub_assets',
				select: 'enum',
				id:'aid'
			}
		}
	}
}