/* @inject: $provide */

// Extend service with easy to use checks

$provide.decorator('CurrentUserService', function($delegate) {

	CUS = $delegate;

	CUS.canFillForm = function() {
		return CUS.hasRole(['Admin', 'FormsFiller', 'FormsAdministrator']);
	}

	CUS.canRaiseForm = function() {
		return CUS.hasRole(['Admin', 'FormsRaiser', 'FormsAdministrator']);
	}

	CUS.canManageForms = function() {
		return CUS.hasRole(['Admin', 'FormsAdministrator']);
	}


	return $delegate;
});