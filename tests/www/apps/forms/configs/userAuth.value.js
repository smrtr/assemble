{
	FormPage: {
		access: ['Admin', 'FormsFiller', 'FormsAdministrator', 'FormsRaiser'],
		onFail: '/'
	},
	FormsPage: {
		access: ['Admin', 'FormsFiller', 'FormsAdministrator'],
		onFail: '/'
	},
	TemplatePage: {
		access: ['Admin', 'FormsAdministrator'],
		onFail: '/'
	},
	TemplatesPage: {
		access: ['Admin', 'FormsRaiser', 'FormsAdministrator'],
		onFail: '/'
	}
}