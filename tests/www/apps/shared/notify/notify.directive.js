/*@inject: $templateCache, $compile, $timeout*/
return {
		scope: {},
    	restrict: 'A',
		controller: function($scope) {
			$scope.type = 'working';
			$scope.show = false;
			$scope.message = null;
		},
    link: function (scope, el, attrs) {

			if(!attrs.notifyDirective) return false;

			var autoHide = true;


			if(angular.isString(attrs.autoHide) && attrs.autoHide.toLowerCase() == 'false') {
				autoHide = false;
			}

			var template = attrs.notifyDirective + "NotifyDirectiveTpl";
			$compile(el.html($templateCache.get(template)).contents())(scope);

			el.attr('id', template);

			scope.$on('notify:working:' + attrs.notifyDirective, function(e, params) {
					scope.show = true;
					scope.type = 'default';
					scope.message = params.message || 'Working...'

					$timeout(function(){
						if(angular.isFunction(params.onComplete)) {
							params.onComplete();
						}
					}, 500);
			});



			function actionHandlers(e, params) {
				scope.show = true;
				scope.type = params.type || 'error';
				scope.message = params.message;

				if(angular.isDefined(params.autoHide)){
					autoHide = params.autoHide;
				}
				return $timeout(function(){
					if(autoHide) scope.show = false;

					if(angular.isFunction(params.onComplete)) {
						params.onComplete();
					}
				}, 3000);
			}

			scope.$on('notify:success:' + attrs.notifyDirective, function(e, params) {
					scope.type = 'success';
					scope.message = params.message;

					return $timeout(function(){
						if(autoHide) scope.show = false;

						if(angular.isFunction(params.onComplete)) {
							params.onComplete();
						}
					}, 3000);
			});

			scope.$on('notify:error:' + attrs.notifyDirective, actionHandlers);




    }
};

