/* @inject: $rootScope, $timeout*/

return {

    template: '<div class="loading-curtain"><div class="loading-hide" ng-transclude></div></div>',
    transclude: true,
    restrict: 'A',
    scope: {
        message: '='
    },
    link: function (scope, element, attrs) {
        if(angular.isDefined(attrs.loadingScreenDirective) && attrs.loadingScreenDirective) {

            var overlayClass   = 'loading-overlay-' + attrs.loadingScreenDirective;
            var hideables = element.find('.loading-hide');
            element.addClass('loading-state');

            hideables.css({opacity: 0, position: 'relative'});
            var overlay = '<div class="loading-overlay '+ overlayClass  +'"><i class="fa fa-spinner fa-spin fa-3x"></i><br/><br/><p>Loading...</p></div>';

            element.append(overlay)
            $rootScope.$on('loading-start:'+attrs.loadingScreenDirective, function(callback) {
                element.addClass('loading-state');
                $('.' + overlayClass).css({opacity: 1});
                hideables.css({opacity: 0, position: 'relative'});
            });

            $rootScope.$on('loading-stop:'+attrs.loadingScreenDirective, function(show) {
                $timeout(function(){
                    $('.' + overlayClass).remove();
                    element.removeClass('loading-state', function() {
                        hideables.fadeTo(700, 1);
                    });
                });
            });

            scope.$on('$destroy', function(){
                element.remove();
            })
        }
    },
};
