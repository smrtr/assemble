/* @inject: $timeout*/

return {

			scope: {
				settings: '=wordCounterDirective'
			},
			require: 'ngModel',
			restrict: 'A',
			priority: 10,
			link: function (scope, el, attrs, ngModel) {
				scope.settings = scope.settings || {};

				if(!angular.isObject(scope.settings)) {
					throw 'wordCounterDirective must be an object';
				}

				var counter = 0;
				var limit = scope.settings.limit || 255;
				var type = scope.settings.type || 'words';

				update = function(value){

					if(angular.isString(value)) {

						el.html(value);
						var text = el.text();

						switch(type) {

							case 'words':
								counter = text.split(' ').length;
								break;
							default:
								counter = text.length;
								break;
						}
						style = "font-weight:bold";
						if(counter > limit) {
							style += ";color:red";
						}

						el.html('<span style="'+ style +'">' + counter + '/' + limit +'</span>');
					}
				}

				ngModel.$render = function() {
						update(ngModel.$viewValue);
				}
			}
};


