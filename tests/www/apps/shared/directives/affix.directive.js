/*@inject: $compile, $rootScope, $window, $timeout*/
return {

    restrict: 'A',
		priority: 20,
    link: function (scope, el, attrs) {

			var window = angular.element($window);
			var offset = el.offset();
			var startingElementHeight = offset.top;
			var startingElementLeft = offset.left;

			scope.config = {offset: 0}

			angular.extend(scope.config, scope.$eval(attrs.affixDirective) || {});

			scope.config.offset = parseInt(scope.config.offset);

			$timeout(function(){
				window.scroll(function(){
					applyAffix();
				});
			});

			function applyAffix() {
				var height = window.height();
				var scrollTop = window.scrollTop();

				if(startingElementHeight - scope.config.offset <= scrollTop) {
					// @todo z-index is abstract
					el.css({position:'fixed', top: scope.config.offset + 'px', width: el.width(), 'z-index': 50});
					el.addClass('affix-directive');
				}
				else {
					el.css({position:'static', top:'0', width: 'auto', 'z-index': 0});
					el.removeClass('affix-directive');
				}

			}

    }
};

