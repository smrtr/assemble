/*@inject: $filter*/
return {
	scope: {
		settings: '=datetimeDirective'
	},
	priority: 1,
	transclude: true,
	template: '<span>{{datetime}}</span>',
	restrict: 'A',
	link: function (scope, el, attrs) {

		var timestamp = scope.settings.timestamp || null;
		var date = new Date();
		scope.format = scope.settings.format || 'medium';

		scope.datetime = $filter('date')(date, scope.format);

	}

};

