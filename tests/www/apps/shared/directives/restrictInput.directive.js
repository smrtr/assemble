return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var regex = false;
        if(attrs.restrictInputDirective) {
            switch(attrs.restrictInputDirective) {
                case 'numeric':
                    regex = /[^0-9+.]/g;
                break;
                case 'alphanumeric':
                    regex = /[^0-9a-zA-Z+.]/g;
                break;
            }
        }

       if(regex) {
        modelCtrl.$parsers.push(function (inputValue) {
            if (inputValue == undefined) return ''; // catch undefined instance
            var transformedInput = inputValue.replace(regex, '');
            if (transformedInput!=inputValue) {
               modelCtrl.$setViewValue(transformedInput);
               modelCtrl.$render();
            }

            return transformedInput;
        });
       }
     }
};