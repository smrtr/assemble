/*
 *@inject: $timeout, $window
 *@name: textarea
 **/
return {

	restrict: 'E',
	link: function( scope , el , attrs ) {


			var threshold    = 35,
					minHeight    = el[0].offsetHeight,
					paddingLeft  = el.css('paddingLeft'),
					paddingRight = el.css('paddingRight');


			var mirror = angular.element('<div></div>').css({
					position:   'absolute',
					top:        -10000,
					left:       -10000,
					width:      el[0].offsetWidth - parseInt(paddingLeft || 0) - parseInt(paddingRight || 0),
					fontSize:   el.css('fontSize'),
					fontFamily: el.css('fontFamily'),
					lineHeight: el.css('lineHeight'),
					resize:     'none'
			});

			angular.element( document.body ).append( mirror );

			var update = function() {
					var times = function(string, number) {
							for (var i = 0, r = ''; i < number; i++) {
									r += string;
							}
							return r;
					}
					var ngModelValue = scope.$eval(attrs.ngModel);

					var val = el.val();
					if(angular.isString(ngModelValue)) {
						val = ngModelValue;
					}

					val = val.replace(/</g, '&lt;')
							.replace(/>/g, '&gt;')
							.replace(/&/g, '&amp;')
							.replace(/\n$/, '<br/>&nbsp;')
							.replace(/\n/g, '<br/>')
							.replace(/\s{2,}/g, function( space ) {
									return times('&nbsp;', space.length - 1) + ' ';
							});

					mirror.html(val );
					el.css( 'height' , Math.max( mirror[0].offsetHeight + threshold , minHeight ) );

					//el.animate({height: Math.max( mirror[0].offsetHeight + threshold , minHeight ) + 'px'}, 500);
			}

			scope.$on('$destroy', function() {
					mirror.remove();
			});

			el.bind( 'keyup keydown keypress change' , update );
			update();
	}

};