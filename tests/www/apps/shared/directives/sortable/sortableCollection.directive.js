/* @inject: $log, $compile, StringService, $templateCache */

var str = StringService;

var directive = {

	restrict: 'A',
	link: function(scope, el, attrs) {

		if(attrs.sortableCollectionDirective) {

			var collection = scope[attrs.sortableCollectionDirective];
			if(angular.isObject(collection)) {
				el.sortable({
					//handle: ".form-element-handle",
					cursor: "move",
					scroll: true,
					delay: 0,
					revert: 0,
					axis:'y',
					placeholder: "sortable-collection-placeholder"
				});

				el.disableSelection();

				el.on( "sortdeactivate", function( event, ui ) {
					var item = angular.element(ui.item);
					var from = item.scope().$index;
					var to = el.children().index(ui.item);

					if(to >= 0){
						scope.$apply(function(){
							if(from >= 0){
								scope.$emit('sortable-collection:sorted', {from:from,to:to});
							}
						})
					}
				});


				scope.$on('sortable-collection:sorted', function(ev, pos) {
					collection.move(pos.from, pos.to);
				});
			}
		}
	}
}



return directive;