
var counter = 0;
return {
	restrict: 'A',
	link: function(scope, el, attr) {
	// config what is item and what is coutme via attrs here 

		var options = scope.$eval(attr.conditionalNumberingDirective);

		var params = {};

		if(angular.isDefined(options.variable)) {
			params = scope[options.variable];
		}else if(angular.isDefined(scope.item)) {
			params = scope.item;
		}

		if(angular.isDefined(scope.val) && angular.isDefined(scope.key)) {
			params = scope.val;
		}

		if(scope.$index === 0) {
			counter = 1;
		}
			
		scope.$numberedIndex = counter;
		
		if(angular.isDefined(params.conditionalNumberingSkip) && params.conditionalNumberingSkip) {
			scope.$numberedIndex = null;;
		}else {
			counter++
		}
	}
}