
return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {

            if(angular.isDefined(attrs.highlightStack)) {
                var stack  = attrs.highlightStack.toLowerCase();
                element.html(stack);
                
                if(angular.isDefined(attrs.highlightNeedle)) {                
                    needle = attrs.highlightNeedle.toLowerCase();
                    index  = stack.indexOf(needle);

                    if ( index >= 0 )
                    { 
                        var html = stack.substring(0, index) + "<span class='text-highlight-yellow'>" + stack.substring(index, index + needle.length) + "</span>" + stack.substring(index + needle.length);
                        element.html(html);
                    }
                }

                
            }   
        }
    };