/* @inject: $parse, $filter, $compile
 * @name: debug
 */
return {
    restrict: 'E',
    link: function (scope, element, attr) {
    	var json = angular.isDefined(attr.flat)? false: true;
			if(attr.val) {
				var model = $parse(attr.val)(scope);
				var value = $filter('json')(model);
				element.html( json? '<pre>' + value + '</pre>': value );
			}
    }
}