
return {
	require: 'ngModel',
	link: function (scope, element, attrs, ngModel) {

		var limit = scope.$eval(attrs.maxlengthDirective);

		if(limit) {
				var maxlength = Number(scope.$eval(attrs.maxlengthDirective));

				function fromUser(text) {
						if (text.length > maxlength) {
							var input = text.substring(0, maxlength);
							ngModel.$setViewValue(input);
							ngModel.$render();
							return input;
						}
						return text;
				}
				ngModel.$parsers.push(fromUser);
		}

	}
};
