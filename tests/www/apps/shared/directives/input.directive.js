/* @inject:  $timeout, $log, $compile, $templateCache, $rootScope */
var stencil = {
	type: 'blank',
	zone: '.form-element-dropzone',
	resolveSettings: function(scope, settings) {
		settings = angular.extend(stencil, scope.$eval(settings));
	}
}

/*

		 * Select suggest
		 *
		 * @uses select2.js select2.angular
		 * @param object q select2 query object
		 * @return object select2 object items

		selectSuggest: function(q){
			var params,items = {results:{}};
			var service = '?';
            var search = null;
			try{
				service+= q.element.context.attributes['data-service'].nodeValue;
                search  = q.element.context.attributes['data-search-field'].nodeValue;
			}catch(e){

			}
			params = {term:q.term};

            if(search && q.term) {
                params.filter = {};
                params.filter[search] = q.term;
            }
			query(service, params).then(function(data){
				items.results = data.response?data.response:[];

                // using new notation we have
                if(angular.isDefined(data.response.rows)) {
                    items.results = data.response.rows;
                }

				q.callback(items);
			});
		},
 */

return {
	scope: {},
	controller: function($scope) {
		$scope.selectSuggest = $rootScope.selectSuggest;
	},
	link:function(scope, el, attrs) {
		var content = el.html( $templateCache.get(attrs.inputTemplate || 'inputDirectiveTpl') ).contents();
		if(!attrs.inputDirective || !content) return false;

		scope.type = attrs.inputDirective;
		$compile(content)(scope);
	}
}