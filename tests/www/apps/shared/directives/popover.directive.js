/* @inject: $compile, $templateCache*/


return {
        restrict: 'A',


        link: function (scope, element, attrs) {

			if(attrs.popoverDirective) {

				$(element).popover({
					html: true,
					content: function() {
						return $compile($templateCache.get(attrs.popoverDirective))(scope);
					}
				}).on('shown.bs.popover', function(e){
					var popover = jQuery(this);
					jQuery(this).parent().find('div.popover .close-popover').on('click', function(e){
						popover.popover('hide');

					});
				});
				
			}


        }
    };