/* @inject:constants */

// use $q for pre and post processing

return Class.extend({

	init: function(){},


	setAllowedExtensions: function(extensions) {

		if(angular.isArray(extensions)) {
			this.allowedExtensions = extensions || [];
		}
	},


	validateFileUpload:  function(files, event) {
		var defaults = {preprocess: null, postprocess: null};
		event = angular.extend(defaults, typeof event === 'object'? event: {});
		var errors = [];
		for (var i = 0; i < files.length; i++) {
			if(typeof event.preprocess === 'function') {
				event.preprocess();
			}
			var file = files[i];

			if(angular.isArray(this.allowedExtensions)) {

				var fileType = this.getTypeFromMime(file.type);
				if(this.allowedExtensions.indexOf(fileType) === -1 ) {
					errors.push( 'Only allowed extensions (' + this.allowedExtensions.join(' | ') + ')');
				}
			}

			else if(constants.files.restrict_file_uploads) {
				if (constants.files.accepted_applications.indexOf(file.type) === -1 || constants.files.unaccepted_applications.indexOf(file.type) != - 1) {
					errors.push(constants.errors.invalid_upload_extension);
				}
			}
			if (file.size > constants.files.max_size) {
					errors.push(constants.errors.max_file_size_exceeded);
			}
			if(typeof event.postprocess === 'function') {
				event.postprocess(file, errors);
			}
		}
	},

	getFileMimeType: function(type){
		return constants.files.mimes[type];
	},

	getTypeFromMime: function(type){
		var mimes = constants.files.mimes;
		for(var i in mimes) {
			if(mimes[i] === type) {
				return i
			}
		}
	}


});