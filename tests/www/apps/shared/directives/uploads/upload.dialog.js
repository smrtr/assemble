/*
 * @inject: $scope, CollectionFactory, constants, $controller, UploadModel, $rootScope
 */

$scope.config = $scope.params;

$scope.file = $scope.config.current;

angular.extend(this, $controller('UploadController', {$scope: $scope}));

$scope.tab = 'fromFile';

$scope.confirm = function () {
	$scope.$close(true);
};

$scope.close = function () {
	$scope.$close(false);
};


$scope.selectFile = function(file) {
    $scope.$close(($scope.selectedFile = file));
}

$scope.searchables = {
	name:{label: 'file name', value: true, field: 'orig_name'},
  category: {label: 'category', value: false, field: 'dir'}
};


$scope.searchParams = { page: 1, term: ''}

$scope.search = function(search) {
    var searchOn = {};
    for(i in $scope.searchables) {
        if($scope.searchables[i].value) {
            searchOn[$scope.searchables[i].field] = search;
        }
    }
    searchOn['search_method'] = 'OR';

    $scope.models.upload.hydrate('inherit').search($scope.searchParams.page, 40, searchOn).then(function(response) {
        if(response) {
            // this scope does not inherit from the rootscope hence why we broadcast instead of emit
            $rootScope.$broadcast('cloak-directive:success:files');
            $scope.files = response;
            $scope.files.items = new CollectionFactory($scope.files.items);
        }else {
            $rootScope.$broadcast('cloak-directive:error:files');
        }
    });
};

$scope.$watch('searchParams.page', function(nv, ov) {
	$scope.search($scope.searchParams.term);
});

