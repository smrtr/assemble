/*
 * @inject: $scope, $controller, CollectionFactory, $upload, UploadModel, constants, UploadFactory, uploadConfigs, $rootScope
 */

$scope.updateModel = function(){}






$scope.selectedFile = null;
$scope.onFileSelect = function(files, params) {

		 params = angular.isObject(params)? params: {};
		 params.returnSource = true;

		 var UF = new UploadFactory;

		 UF.setAllowedExtensions($scope.config.allowedExtensions);

     UF.validateFileUpload(files, {
         preprocess: function() {
           	$scope.file = false;
            $scope.error = '';
         },
         postprocess: function(file, errors) {
            if(!errors.length) {
                $scope.upload = $upload.upload({
                    url: constants.paths.u_api + '?s=api&t=upload&q=create',
                    data: params,
                    file: file
                }).success(function(data, status, headers, config) {
										console.log(data);
                    return data.response;
                })
								.then(function(response){

									if(response.data.response) {

										UploadModel.hydrate('row').getOne(response.data.response).then(function(data) {
												if(data.response) {
														// this scope does not inherit from the rootscope hence why we broadcast instead of emit
														$rootScope.$broadcast('cloak-directive:success:files');
														$scope.file = data.response;

														$scope.updateModel($scope.file);

												}else {
														$rootScope.$broadcast('cloak-directive:error:files');
												}
										});
									}else{
										$scope.error = 'Unknown error. Please refresh the page.';
									}

								});
            }else{
                $scope.error = errors.join(" <br/>");
            }
         }
     });
};

$scope.selectFile = function(file) {
	$scope.selectedFile = file;
}

$scope.remove = function(id) {
	//UploadModel.remove(id)
}