/*@inject: $rootScope, uploadConfigs, $q*/


return {
	scope: {
		settings: '=uploadDirective'
	},
	restrict: 'A',
	replace: true,
	templateUrl: 'uploadDirectiveTpl', // maybe mixing responsibilities inside here
	controller: 'UploadController',
	require: 'ngModel',
	link: function (scope, element, attrs, ngModel) {

			scope.config = { dialog: true, params: uploadConfigs }

			angular.extend(scope.config, scope.settings || {});

			scope.file = scope.config.current;

			scope.updateModel = function(data) {
				if(data) {
					ngModel.$setViewValue(data);
				}
			}

			ngModel.$render = function() {
				scope.file = ngModel.$viewValue;
			}

			scope.upload = function() {
				$rootScope.dialog('upload', scope.config, {blackout:true}).then(function(response){
					if(response) {
						scope.updateModel(response);
					}
				});
			}

			scope.remove = function(){

				var d = $q.defer();

				d.resolve(scope.file = false);

				d.promise
				.then(function() {
					return angular.isFunction(scope.config.remove)? scope.config.remove(): angular.noop;
				})

			}
	}
}
