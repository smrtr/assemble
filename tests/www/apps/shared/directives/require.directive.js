/*@inject: $templateCache, $compile, $rootScope, HelpersFactory*/
return {
	scope: false,
	transclude: true,
	restrict: 'A',
	link: function (scope, el, attrs) {
		var defaultTemplate = $templateCache.get(attrs.requireDefault);

		scope.$root = $rootScope;

		attrs.$observe('requireDirective', function(nv, ov){
			var template = $templateCache.get(nv);
			if(template) {
				$compile(el.html(template).contents())(scope);
			}else if(defaultTemplate) {
				$compile(el.html(defaultTemplate).contents())(scope);
			}else{
				$compile(el.html(null).contents())(scope);
			}
		});
	}
};

