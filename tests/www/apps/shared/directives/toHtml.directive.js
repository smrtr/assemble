
return {
		scope: {
			html: '=toHtmlDirective'
		},
    restrict: 'A',
    link: function (scope, el, attrs) {
			scope.$watch('html', function(){
				el.html(scope.html);
			});
    }
};

