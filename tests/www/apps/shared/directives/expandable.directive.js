/* @inject: $timeout*/

return {

        template: '<div class="cursor" title="collapse" ng-show="!open" ng-click="open = true">{{title}} <i class="pull-right fa fa-minus"></i></div>'
                + '<div class="cursor" title="expand" ng-show="open" ng-click="open = false">{{title}} <i class="pull-right fa fa-plus"></i></div>',

        scope: {
            open: '@',
            title: '@'
        },
        transclude: true,
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, model) {
            var defaultState = false;

            if(angular.isDefined(attrs.defaultState)) {
                if(attrs.defaultState === 'open') defaultState = false;
                if(attrs.defaultState === 'closed') defaultState = true;
            }

            $timeout(function() {
                if(model.$viewValue) {
                    scope.open = model.$viewValue;
                }else{
                    scope.open = defaultState;
                }
            });

            scope.$watch('open', function(nv, ov) {

                if(angular.isDefined(nv)) {
                    model.$setViewValue(nv);
                }else{
                    //model.$setViewValue(false);
                }
            });
        },
    };


