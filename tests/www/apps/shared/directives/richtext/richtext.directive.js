
return {
	scope: {
		content: '=richtextDirective'
	},
	restrict: 'A',
	replace: true,
	templateUrl: 'richtextDirectiveTpl',
	link: function (scope, element, attrs) {

		scope.originalHeight = element.outerHeight();
		scope.editMode = false;
		scope.autoexpand = !('autoexpand' in attrs) || attrs['autoexpand'] !== 'off';

		scope.toggleEditMode = function() {
			scope.editMode = !scope.editMode;
		};
		

		scope.execCommand = function (command, options) {
			if(command ==='createlink'){
				options = prompt('Please enter the URL', 'http://');
			}
			scope.$emit('execCommand', {command: command, options: options});
		};
	}
}
