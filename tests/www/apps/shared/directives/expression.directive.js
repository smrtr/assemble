/*@inject: $compile, $rootScope, $window, $timeout*/
return {

    restrict: 'A',
		priority: 20,
    link: function (scope, el, attrs) {

			scope.extend = function(dst, src) {

				dst = angular.extend(dst, src);
			}

			scope.$eval(attrs.expressionDirective);

    }
};

