/* @inject: $log*/

return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      
      if(angular.isDefined(attrs.ternaryDirective)) {
   
        var models = attrs.ternaryDirective.split(/[\s\&\|,]+/);
        angular.forEach(models, function (model) {

            if(angular.isString(model)) {
                scope.$watch(model, function (nv) {
                    var result = (nv? attrs.ifTrue: attrs.ifFalse);
                    element.html(result? result: attrs.otherwise);
                });
            }
        });
        
      } else $log.error('No condition set');
    },
};