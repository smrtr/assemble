return function (array, value) {
	
	if(typeof array === 'object') {
		return (array.indexOf(value) !== -1);
	}
	return false;
  
};