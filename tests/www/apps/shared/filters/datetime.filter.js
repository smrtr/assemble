/*
 * @inject: $filter
 */
return function (date, format) {

    if(date === undefined || date === null || !date) {
        return null;
    }

    var parts = date.split(' ');
    format = format === undefined?'mediumDate': format;
    return $filter('date')(parts[0], format) + ' - ' + parts[1];
};