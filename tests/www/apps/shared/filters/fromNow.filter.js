return function(date) {
    return moment(date).fromNow();
}