/*
 * @inject: $rootScope, constants
 */
return function (priority) {

    if(angular.isDefined(constants.priorities)) {
        if(angular.isDefined(constants.priorities)) {
            var priorities = constants.priorities;
            if(angular.isDefined(priorities[priority])) {
                return priorities[priority].text;
            }
        }
    }
    return priority;
};