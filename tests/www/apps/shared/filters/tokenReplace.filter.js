/*
 * @inject: ObjectFactory, systemTokens
 */
var map = systemTokens;

return function (str, src) {

	if(!str) return '';

	for(var i in map) {
		var key = map[i];
		var result = ObjectFactory.objectValue(key, src);
		str = str.replace('$' + i + '$', result? result: '$' + i + '$');

	}
	return str;

};