return function (input, defaultValue) {
    if (input == undefined || input == null || input =='') {
        return defaultValue;
    } else {
        return input;
    }
};