/*
 * @inject: CurrentUserService, $routeParams, $route, userAuth, $location, $rootScope, userForward
 */

var CurrentUser;

authoriseRoutes = function (controller) {
	if(angular.isDefined(userAuth[controller])) {
		var configs = userAuth[controller];
		if(angular.isDefined(configs.access) && angular.isArray(configs.access)) {
			if(!CurrentUser.hasRole(configs.access)) {
				var onFail = '403';
				$rootScope.$broadcast('notify:error:globalStatic', {type:'danger', autoHide:true, message: 'You do not have the right privileges to access that page'});

				if(angular.isDefined(configs.onFail) && configs.onFail) {
					onFail = configs.onFail;
				}
				return $location.path(onFail);
			}
		}
	}
	return false;
}
$rootScope.$on('$routeChangeStart', function(next, current) {
	CurrentUserService.prepareRepositry().then(function(_CurrentUser) {

		$rootScope.currentUser = CurrentUser = _CurrentUser;
		var privs = CurrentUser.get('privs') || {};
		for(var i in privs) {
			if(userForward[i]) {
				return $location.path(userForward[i]);
			}
		}
		authoriseRoutes(next.targetScope.$routeState.controller);
	});

	if(angular.isDefined(CurrentUser)) {
		authoriseRoutes(next.targetScope.$routeState.controller);
	}
});