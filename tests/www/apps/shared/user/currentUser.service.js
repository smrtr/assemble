/*
 * @inject: BasePresenter, UserModel, constants
 */

var data = {
	details: constants.user
};

var UserPresenter = BasePresenter.extend({

	privCheck: function privCheck(privs, stack) {
		if(angular.isArray(privs) && angular.isObject(stack)) {
			for(i in privs) {
				if(angular.isDefined(stack[privs[i]])) {
					return true;
				}
			}
		}
		return false;
	},
	
	hasRole: function(privs) {
		var _this = this;
		// _this.data.privs = {'FormsFiller': 'FormsFiller'}
		//if data already exists then don't go and fetch;
		if(_this.data.privs) {
			return _this.privCheck(privs, _this.data.privs);
		}
		return false;
	}
});

User = new UserPresenter(data)

User.registerRepositryDataSources({
	'privs': UserModel.getPrivileges(data.details.id).then(function(data){
		return data.response;
	})
});

return User;
