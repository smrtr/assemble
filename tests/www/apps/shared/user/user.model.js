/*
 * @inject: BaseModel
 */

var model = new BaseModel('user');

model.getPrivileges = function(id) {
	return model.base('privileges', {id: id});
}

return model;
