/* @inject: $provide*/

// intercept the switch case to make it allow multiple conditions

$provide.decorator('ngSwitchWhenDirective', function($delegate) {
    $delegate[0].compile = function(el, attrs, transclude) {
        return function(scope, el, attr, ctrl) {
            var subCases = [attrs.ngSwitchWhen];

            if(attrs.ngSwitchWhen && attrs.ngSwitchWhen.length > 0 && attrs.ngSwitchWhen.indexOf('||') != -1) {
                subCases = attrs.ngSwitchWhen.split('||');
            }
            var i = 0, switchCase;

            while(i < subCases.length) {
                switchCase = $.trim(subCases[i++]);
                ctrl.cases['!' + switchCase] = (ctrl.cases['!' + switchCase] || []);
                ctrl.cases['!' + switchCase].push({ transclude: transclude, element: el });
            }
        }
    }
    return $delegate;
});