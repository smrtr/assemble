/*
 * @inject: $rootScope, dialogs, $modal, constants
 */ 

var modalProvider = function (scope, options) {

    //options.windowClass += '';

    options.windowClass = [];
    if(options.blackout) {
        options.windowClass.push('modal-blackout');
    }
    return $modal
		.open({
			scope: scope,
			keyboard: options.keyboard || true,
			backdrop: options.backdrop || false,
			dialogFade: true,//options.fade || false,
			templateUrl: options.templateUrl,
			controller: options.controller,
			windowClass: options.windowClass.join(' ')
		})
		.result;
}

var base = function(slug, params, options, type) {
	console.log('opening... ' + slug); 
	if(slug && angular.isDefined(dialogs[slug])) {

        options = options || {};
        var scope = $rootScope.$new();
            scope.params = params;
            scope.constants = constants;

        options.templateUrl = dialogs[slug].templateUrl;
        options.controller  = dialogs[slug].controller;

        switch(type) {
            case 'dialog':
            case 'modal':
                return modalProvider(scope, options);;
            case 'dialog':
                //return dialogProvider(scope, options);;
        }

        return console.log('Dialog type does not exist');
	}else {
		console.log('Dialog does not exist');
		return false;
	}
	return false;
};


$rootScope.dialog = function(slug, params, options) {
	return base(slug, params, options, 'dialog');
};
$rootScope.modal  = function(slug, params, options) {
	return base(slug, params, options, 'modal');
};