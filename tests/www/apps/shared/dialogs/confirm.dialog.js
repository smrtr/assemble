/*
 * @inject: $scope
 */

$scope.confirm = function () {
	return $scope.$close(true);
};

$scope.close = function () {
	return $scope.$close(false);
};