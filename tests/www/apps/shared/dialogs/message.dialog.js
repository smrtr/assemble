/*
 * @inject: $scope
 */
$scope.params.style = $scope.params.style || 'middle';
$scope.params.header = $scope.params.header || false;
$scope.params.body = $scope.params.body || false;
$scope.params.confirmText = $scope.params.confirmText || 'yes';
$scope.confirm = function () {
	return $scope.$close(true);
};

$scope.close = function () {
	return $scope.$close(false);
};