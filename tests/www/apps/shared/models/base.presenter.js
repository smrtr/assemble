/* @inject: $q
 *
 */

/* Using the presenter layer instead of model for two reasons.
1) BaseModel is wrongly acting as a service but name is already taken
2) Will attempt to break dependency of view by not including things like $index++ but rather have getNumber();
Includes behaviour of a specific
*/

var BasePresenter = Class.extend({
	init: function(data) {
      this.data = data;
	},
	getId: function(){
		return this.get('id');
	},
	get: function(prop){
		return this.data[prop];
	},
	set: function(prop, value){
		return this.data[prop] = value;
	},
	is: function(prop, value){
		return this.data[prop] === value;
	},

	registerRepositryDataSources: function(dataSources) {
		this.dataSources = dataSources;
	},
	// should return promise
	prepareRepositry: function() {
		var deferred = $q.defer(), _this = this;

		function _prepareRepo() {
			deferred.notify('About to greet ');
			if(!_this.repositryLoaded) {
				for(var i in _this.dataSources) {
					_this.dataSources[i].then(function(data){
						_this.data[i] = data;
						deferred.resolve(_this);
					});
				}
				_this.repositryLoaded = true;
			}

			return _this;
		}
		
		_prepareRepo();
		
		return deferred.promise;
	}
});

return BasePresenter;