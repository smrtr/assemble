/*
 * @inject: ResourceApi, $q, constants
 */

return function (name) {
        var model = {
			name: name,
			hydrate_method: constants.api_hydrate_method,
			allowed_hydrate_methods: ['var','row','rows','inherit']
		};

		model.hydrate = function(method) {
			if(model.allowed_hydrate_methods.indexOf(method) != -1) {
				model.hydrate_method = method;
			}
			return model;
		},

		/**
		 * Base model
		 *
		 *
		 * @param method string
		 * @param params object
		 * @param callbacks object callbacks
		 * @return object
		 */
		model.base = function (method, params) {

			var endpoint = constants.paths.u_api + '?s=api&t=' + model.name + '&q=' + method + '&_ref';

			callbacks = typeof callbacks === 'object'? callbacks: {};

			if(angular.isObject(params)) {
		        params['1t'] = true;
		        params['api_hydrate_method'] = model.hydrate_method;
		    }
		    else {
		    	params = {
			        '1t': true,
			        'api_hydrate_method': model.hydrate_method
		    	}
		    }

			return ResourceApi.post(endpoint, params).then(function(data){
				return data;
			});
		};

		/**
		 * Get one
		 *
		 * @param integer id
		 * @param object callbacks
		 * @return object
		 */
		model.getOne = function(id, select) {
			return model.base('get_one', {'id': id, select:select});
		};

        /**
		 * Get object
		 *
		 * @param integer id
		 * @param object callbacks
		 * @return object
		 */
		model.getObject = function(id) {
			return model.base('object', {'id': id });
		};

		/**
		 * Create
		 *
		 * @param params object
		 * @return object
		 */
		model.create = function (params) {
			return model.base('create', {'array': params});
		};

		/**
		 * Get
		 *
		 * @param params object
		 * @return object
		 */
		model.get = function (params) {
			params = typeof params === 'object' ? params: {filter: {id: parseInt(params)}};
			return model.base('get', params);
		};

		/**
		 * Get collection
		 *
		 * @param params object
		 * @return object
		 */
		model.getCollection = function (p, rpp, filter, select, type) {

			filter = filter || {};
			select = select || [];
            var p = p || 1;
			params = { page: p || 1, rpp: rpp || 10, filter: filter, select:select};
			type   = type   || 'get';

			// we want the data in a particular format
			return model.base(type, params).then( function(data) {

				var obj = {
                    page: p,
                    p: data.pagination,
                    items: data.response.rows
                }

				return obj;
			});
		};

		/**
		 * Destroy
		 *
		 * @param params object
		 * @return object
		 */
		model.destroy = function (id) {
			params = { id: parseInt(id) };
			return model.base('destroy', params);
		};

		/**
		 * Update
		 *
		 * @param params object
		 * @return object
		 */
		model.update = function (id, params) {
			return model.base('update', {'array': params, id:id} );
		}


        /**
		 * Search
		 * order of params not particularly helpful
		 * @param params object
		 * @return object
		 */
		model.search = function(p, rpp, filter, select) {
            return model.getCollection(p, rpp, filter, select, 'search');
		}

		return model;
}