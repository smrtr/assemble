/*
 * @inject: $http, $q, $rootScope
 */
var apiActionHandler = function(data) {
	switch(data.action){
		case 'LOGOUT': return $rootScope.dialog('SessionExpiredDialog');
		default:       return false;
	}
	return true;
};

post = function(query, params) {
	var d = $q.defer();
	$http.post(query, params).success(function (data) {
		if(apiActionHandler(data)) {
            return false;
        }
		console.log(query, params)
		console.log(data);
		return d.resolve(data);
	});
	return d.promise;
}

var methods = {

	post:	 post,
	//get:	 get,
	//put:	 put,
	//destroy: destroy
};

return methods;