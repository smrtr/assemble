/*@inject: $templateCache, $compile*/
return {
    template: '<div class="loading-hide" ng-transclude></div>',
    transclude: true,
    restrict: 'A',
    link: function (scope, element, attrs) {
        if(attrs.cloakDirective) {

        	var templateHtml = attrs.templateOnLoad? $templateCache.get(attrs.templateOnLoad): false;
        	templateHtml = templateHtml? templateHtml: 'Loading...';

					var overlayClass = 'loading-overlay-' + attrs.cloakDirective;
					var overlay = '<div class="loading-overlay '+ overlayClass +'">' + templateHtml + '</div>';
					var containerID = 'cloak-' + attrs.cloakDirective;
					var hideables = element.find('.loading-hide');

					element.attr('id', containerID).addClass('loading-state');
					element.append(overlay);
					hideables.css({opacity: 0, position: 'relative'});

					scope.$on('cloak-directive:success:'+attrs.cloakDirective, function(e, params) {
						$('.' + overlayClass).remove();
						element.removeClass('loading-state', function() {
							$('#'+containerID+'>.loading-hide').fadeTo(700, 1);
						});
					});

					scope.$on('cloak-directive:error:'+attrs.cloakDirective, function(e, params) {
						var templateHtml = attrs.templateOnFail? $templateCache.get(attrs.templateOnFail): false;

						templateHtml = templateHtml? templateHtml: 'Error 404!';

						if(templateHtml) {
							var $newScope = angular.extend(scope.$new(true), params);
							$compile(element.html( templateHtml ).contents())($newScope);
							$('.' + overlayClass).remove();
							element.removeClass('loading-state');
						}
					});
				}
    }
};

