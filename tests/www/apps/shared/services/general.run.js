/* @inject: $rootScope, $routeParams, $location, $window, HelpersFactory, constants, BaseModel, DomService*/


// port to domservice
function initialiseWindowParams() {
    var w = angular.element($window);
    $rootScope.windowDimensions = {
        'h': w.height(),
        'w': w.width()
    };
}

initialiseWindowParams();

angular.element($window).bind('resize', function () {
    initialiseWindowParams();
    $rootScope.$apply();
});


$rootScope.$on("$routeChangeStart", function (event, next, current) {
    if(angular.isDefined(next.$$route)) {
        $rootScope.$routeState = {
            state: next.$$route.state,
            layout: next.$$route.layout,
            absUrl: $location.$$absUrl,
            url: $location.$$url,
            templateUrl: next.$$route.templateUrl,
            viewUrl: next.$$route.viewUrl,
            params: $routeParams,
            controller: next.$$route.controller,
            parent: /^(.+)\.[^.]+$/.exec(next.$$route.state)
        };
    }
});

$rootScope.convertToArray = function(object) {
    return HelpersFactory.convertToArray(object);
};

$rootScope.countArray = function(object) {
    return $rootScope.convertToArray(object).length;
};

// may not work
$rootScope.constants = constants;

$rootScope.select2Suggest = DomService.select2ApiServiceConfig;