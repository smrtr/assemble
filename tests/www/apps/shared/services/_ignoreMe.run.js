/*
 * @inject: $rootScope, $modal, $http, $templateCache, constants
 */

var modalProvider = function (scope, options) {
    return $modal
		.open({
			scope: scope,
			keyboard: options.keyboard || true,
			backdrop: options.backdrop || false,
			dialogFade: true,//options.fade || false,
			templateUrl: options.templateUrl,
			controller: options.controller,
			windowClass: options.windowClass || null
		})
		.result;
}

console.log(dialogs)


var base = function(slug, params, options, type) {
	console.log('opening... ' + slug);
	if(slug && angular.isDefined(dialogs[slug])) {

		options = options || {};

		var scope = $rootScope.$new();
		scope.params = params;
		scope.constants = constants;
		options.templateUrl = dialogs[slug].templateUrl;
		options.controller  = dialogs[slug].controller;
		return modalProvider(scope, options);


	}else {
		console.log('Dialog does not exist');
		return false;
	}
	return false;
};


$rootScope.dialog = function(slug, params, options) {
	return base(slug, params, options, 'dialog');
};
$rootScope.modal  = function(slug, params, options) {
	return base(slug, params, options, 'modal');
};