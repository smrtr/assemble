/* @inject: $log, $timeout, BaseModel*/


	this.scroll = function(selector, speed) {

		return $timeout(function(){
			var el = angular.element(selector);
			angular.element('body').animate({scrollTop: el.offset().top - 50}, speed || 'fast');
			el.addClass('notify-new-dom-element');
			return el;
		}, 1000).then(function(el){
			$timeout(function(){
				el.removeClass('notify-new-dom-element');
			}, 2000);

			return el;
		})
	}

	// this needs redoing
	this.select2ApiServiceConfig = {
		initSelection : function (element, callback) {
			var data = null;
			callback(data);
		},

		query: function(q) {
			var items = {results: []};


			//console.log(q.element.data('search'))
			//return false;
			
			var tokens = ['search', 'method', 'search-on', 'select'];
			var params = {model: null, search: {}, select: 'select2', method: 'search'};

			for(var i in tokens) {
				if((value = q.element.data(tokens[i]))) {
					switch(tokens[i]) {

						case 'search':
							params.model  = value;
							break;
						case 'method':
							params.method = value;
						break;
						case 'select':
							if(angular.isString(value)) params.select += ',' + value;
						break;
						case 'search-on':
							if(angular.isString(value)) {
								params.select += ',' + value;
								angular.forEach(value.split(','), function(value, key) {
									params.search[value.trim()] = q.term;
								});
							}
						break;
					}
				}
				
			}

			params.search.search_method = 'OR';

			console.log('teetette',params);

			var instance = new BaseModel(params.model);
			instance.base(params.method, {filter: params.search, select: params.select, rpp:100}).then(function(data) {

				if(data.response) {
					items.results = data.response.rows;
				}else{
					items.results = [];
				}
				
				q.callback(items);
			});
		}
};