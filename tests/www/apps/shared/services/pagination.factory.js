/* @inject: $http, HelpersFactory*/


return {
	init: function (array, rpp, filterType) {

		var params = {
			pages: 0,
			rpp: rpp || 10,
			offset: 0,
			limit: 0,
			page: 1,
			items: HelpersFactory.convertToArray(array), // origin
			returned: [],
			filtered: [],
			filter: null,
			filterLength: 0,
			filterType: filterType || 'and'
		}

		params.total = params.items.length;

		// calculate offset
		function calculateOffset(page) {
			params.page   = page;
			params.offset = (page - 1) * params.rpp;
			params.limit  = params.offset + params.rpp;
		}

		// check if filter is activated
		function hasFilter() {
			return params.filterLength;
		}

		// Match filter
		function filterMatched(object) {

			var matchesRequired = params.filterLength;
			var matchCount = 0;

			for(i in params.filter) {

                var operator = '=';

                if(angular.isObject(params.filter[i])) {
                    if(angular.isDefined(params.filter[i].value)) {
                        var val = params.filter[i].value;
                    }else return false;

                    if(angular.isDefined(params.filter[i].operator)) {
                        operator = params.filter[i].operator;
                    }else operator = '=';
                } else var val = params.filter[i]

				var key = i;

				if(!angular.isDefined(object[key])) return false;


                // make this callable functions
                if(operator === '=') {
                    if(object[key] == val) matchCount++;
                }else if(operator === 'like') {
                    val.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    if(object[key].match(new RegExp(val, 'i'))) matchCount++;
                }
			}
			return (matchesRequired === matchCount);
		}

		// Filter items. We are rebuilding the filtered list
		function filterItems() {

			params.filtered = [];

			if(!hasFilter()) {
				params.filtered = params.items;
				params.total    = params.filtered.length;
				calculateOffset(params.page);
				return params.filtered;
			}

			for (var i = 0; i < params.items.length; i++) {
				if(filterMatched(params.items[i])) {
					params.filtered.push(params.items[i]);
				}
			}

			// reset limit
			params.total = params.filtered.length;
			calculateOffset(params.page);
			return params.filtered;
		}

		// set the filter. covert to array for convenience
		function setFilter(filter) {
			params.filterLength = HelpersFactory.convertToArray(filter).length;
			params.filter = filter
		}

		return {
			params: function () {
				return params;
			},
			get: function (page, filter) {

				setFilter(filter);
				calculateOffset(page);
				filterItems(page);

				params.returned = [];
				for (var i = params.offset; i < params.limit; i++) {
					if(i === params.total) return params;
					params.returned.push(params.filtered[i]);
				}
				return params;
			}

		};
	}
}