/*unserscore js would come in handy here*/
var helpers = {};

helpers.convertToArray = function(object) {

	var newArray = [];

	if(angular.isArray(object)) return object;

	if(angular.isObject(object)) {
		for(var i in object) {

            if(object[i]) {
				newArray.push(object[i]);
			}
		}
	}
	return newArray;
}

helpers.countArray = function(object) {
	return convertToArray(object).length;
}


helpers.getTopScope = function(currentScope){
	var scope = currentScope;
	while (scope && scope.$parent !== currentScope.$root) {
		scope = scope.$parent;
		return scope;
	}
}

return helpers;