/* @inject: $http */


 var Session = {
    data: {},
    saveSession: function() { /* save session data to db */ },
    updateSession: function() {
      /* load data from db */
      Session.data = {
          toggle: {}
      };
    }
  };
  Session.updateSession();

  return Session;