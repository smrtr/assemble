/*@inject: $timeout*/
// very basic generic poll callback
var poll = {
	interval: 60000,
	instance: null,
	launch: function(callback) {
		return this.instance = $timeout(function() {
			if(typeof callback === 'function') {
				callback();
			}
			return poll.launch(callback)
		}, poll.interval);
	},
	cancel: function(){
		$timeout.cancel(this.instance);
	}
}
return poll;