/*@inject: $log*/
return Class.extend({
	/**
	 * @param object filter
	 */
	filter: {},

	/**
	 * @param bool if key exists inside filter then
	 */
	requiredMatches: 0,

	/**
	 * @param object filter
	 */
	defaultOperator: '=',

	/**
	 * @param object filter
	 */
	operators: {},

	/**
	 * Construct
	 * @return void
	 */
	init: function(filter, operators)
	{
		this.operators = operators;
		this.filter = this.constructFilter(filter);
		this.requiredMatches = this.filter.length;
	},

 /**
	* Filter collection
	*
	* @param  array items
	* @return array filtered items
	*/
	filterCollection: function(items)
	{
		var filtered = [];
		for(var i in items)
		{
			if(angular.isDefined(items[i].properties) && typeof items[i].propertyExists === 'function')
			{
				if(this.match(items[i].properties))
				{
					filtered.push(items[i]);
				}
			}
		}
		return filtered;
	},

 /**
	* Construct filter
	*
	* @param  object   filter
	* @return function predicate
	*/
	constructFilter: function (filter)
	{
		var object = [];
		filter = angular.isObject(filter)? filter: {};
		for(i in filter)
		{
			if(filter[i])
			{
				object.push({value: filter[i], key: i, operator: this.getOperator(i)});
			}
		}
		return object;
	},

 /**
	* Get operator
	*
	* @param  string   operator
	* @return function predicate
	*/
	getOperator: function (operator)
	{
		if(angular.isObject(this.operators))
		{
			if(angular.isDefined(this.operators[operator]))
			{
				return this.operators[operator];
			}
		}
		return this.defaultOperator;
	},

 /**
	* Perform a match
  *
	* @param  string   operator
	* @return function predicate
	*/
  match: function(object)
	{
		var matches = 0;

		for(i in this.filter)
		{
			var key = this.filter[i].key;

			if(!angular.isDefined(object[key])) return false;

			if(this.predicate(this.filter[i].operator)(object[key], this.filter[i].value))
			{
			  matches++;
		  }
		}
	
		return (this.requiredMatches === matches);
	},

  /**
	 * Predicate
	 *
	 * @param  string   operator
	 * @return function predicate
	 */
	predicate: function (operator)
	{
		if(angular.isDefined(this.predicates[operator]) && typeof this.predicates[operator] === 'function')
		{
			return this.predicates[operator];
		}
		return angular.noop;
	},


	predicates: {
		/**
		 * @param  value mixed
		 * @param  test  mixed
		 * @return bool
		 */
		'=': function(value, test)
		{
			return (value == test);
		},

		/**
		 * @param  value mixed
		 * @param  test  mixed
		 * @return bool
		 */
		'like': function(value, test)
		{
			return (value.match(new RegExp(test.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'i')))
		}
	}

});