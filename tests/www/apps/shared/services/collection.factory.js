/*@inject: FilterFactory, $q*/
var events = {};
return Class.extend({
	/**
	 * @var integer auto increment
	 */
	autoIncrement: 0,

	/**
	 * @var object headers
	 */
	headers: {},

	/**
	 * @var object of pre and post processed items
	 */
	origin: [],

	/**
	 * @var object of pre and post processed items
	 */
	processed: [],

	/**
	 * @var array filtered items
	 */
	filtered: false,

	selected: false,

	store: {},
	fieldMeta: {
		name: {operator: 'like'},
	},

	page: 1,

	/**
	 * @param object pagination
	 */
	pagination: { rpp: 20, offset: 0, limit: 0, page: 0 },

	/**
	 * Construct
	 *
	 * @param  object items
	 * @return void
	 */
	init: function(items, viaProxy)
	{
		this.destroy();
		viaProxy? this.setItemsProxy(items): this.setItems(items);
	},

	/**
	 * Get total items
	 */
	total: function()
	{
		return this.origin.length;
		return this.origin.length;
	},

	events: {

	},

	registerEvent: function(key, closure) {
		if(angular.isString(key) && angular.isFunction(closure)) {
			this.events[key] = closure;
		}
	},

	/**
	 * Action hooks
	 *
	 */
	event: function(event, params) {
		//console.log('Action::' + event, params)

		if(angular.isDefined(this.events[event]) && angular.isFunction(this.events[event])) {
			return this.events[event](params, this);
		}
		return params;
	},

	/**
	 * Add an item
	 *
	 * @param  object properties
	 * @return void
	 */
	add: function(properties)
	{
		if(angular.isObject(properties))
		{
			properties = this.cleanItem(properties);
			properties.$id = this.autoIncrement++;
			properties.$selected = false;
			this.origin.push(properties);
		}

		return properties;
	},

	/**
	 * Proxy into a method to gain access to hooks
	 * @param  string method
	 * @param  object params
	 * @return void
	 */
	proxy: function(method, params) {

		var d = $q.defer(), _this = this;
		if(angular.isFunction(this[method])) {

			d.resolve(_this.event('pre:' + method, params));

			// handle the promise
			return d.promise
			.then(function(params) {
				return _this[method](params);
			})
			.then(function(params) {
				return _this.event('post:' + method, params);
			});
		}

		return d.promise;
	},

	/**
	 * Set items/ load items into collection
	 *
	 * @param  array
	 * @return void
	 */
	setItemsProxy: function(items)
	{
		if(angular.isObject(items) || angular.isArray(items))
		{
			for(var i in items)
			{
				this.proxy('add', items[i]);
			}
		}
	},

	/**
	 * Set items/ load items into collection
	 *
	 * @param  array
	 * @return void
	 */
	setItems: function(items)
	{
		if(angular.isObject(items) || angular.isArray(items))
		{
			for(var i in items)
			{
				this.add(items[i]);
			}
		}
	},

	// remove
	remove: function(where)
	{

	},

	/**
	 * Get by a condition, also have where etc
	 */
	getById: function (id)
	{
		if(angular.isDefined(id) && angular.isDefined(this.origin[id]))
		{
			return this.origin[id];
		}
		return false;
	},

	/**
	 * Get by a condition, also have where etc
	 *
	 * @param integer id
	 * @return boolean
	 */
	removeById: function (id)
	{
		if(angular.isDefined(id) && angular.isDefined(this.origin[id]))
		{
			return this.origin.splice(id, 1);
		}
		return false;
	},

	/**
	 * Filter
	 *
	 * @param  object filter object
	 * @return array
	 */
	filter: function(filter, operators)
	{
		this.filtered = false;
		if(angular.isObject(filter))
		{
			var Filter = new FilterFactory(filter, operators);
			this.filtered = true;
			return Filter.filterCollection(this.origin);
		}
		return this.origin;
	},

	/**
	 * Get items
	 *
	 * @param  object where condition
	 * @return array
	 */
	get: function(filter, page)
	{
		//this.origin = this.filter(filter, {name: 'like'});
		//this.origin = this.processPagination(page); pagination not working right
		return this.origin;
	},

	/**
	 * Destroy all items
	 *
	 * @return void
	 */
	destroy: function()
	{
		this.origin = [];
		this.origin = [];
	},

	/**
	 * Move the item
	 *
	 * @param
	 * @return
	 */
	move: function (from, to)
	{
		if(this.origin.length && from >= 0 && to >= 0) {
			return this.origin.splice(to, 0, this.origin.splice(from, 1)[0]);
		}
		return false;
	},

	resolveArguments: function(map, fallback) {

		var reMap = false, keys = [];
		if(angular.isObject(map)) {
			for(var i in map) {
				keys.push(i);
				if(!angular.isDefined(map[i])) {
					reMap = true;
				}
			}
		}

		if(reMap) {
			map = {};
			for(var i in keys) {
				map[keys[i]] = fallback[keys[i]];
			}
		}

		return map;
	},

	/**
	 * Move the item
	 *
	 * @param
	 * @return
	 */
	insert: function (item, position)
	{
		var args = this.resolveArguments({item:item, position:position}, item), _this = this;
		// add the item to the list first and then move it


		this.proxy('add', args.item).then(function(){

			if(_this.origin.length && angular.isNumber(args.position)) {
				_this.move(_this.origin.length - 1, args.position + 1);
			}
			return args;
		});


		return false;
	},

	/**
	 * Process pagination
	 *
	 * @param  integer page
	 * @return array
	 */
	processPagination: function(page)
	{
		if(angular.isNumber(this.page))
		{
			this.pagination.offset = (this.page - 1) * this.pagination.rpp;
			this.pagination.limit  = this.pagination.offset + this.pagination.rpp;
			this.pagination.total  = this.origin.length;

			var items = [];
			for (var i = this.pagination.offset; i < this.pagination.limit; i++)
			{
				if(i === this.total()) return items;
				items.push(this.origin[i]);
			}
		}

		return items;
	},

	/**
	 * Iterator
	 *
	 * @param  string  column
	 * @param  string  callback
	 * @return array
	 */
	iterator: function(callback, context)
	{
		angular.forEach(this.origin, function(row, key)
		{
			if(angular.isDefined(callback) && typeof callback === 'function')
			{
				callback(key, row);
			}
		}, context);
	},

	/**
	 * Get a column
	 *
	 * @param  string column
	 * @return array
	 */
	column: function(column)
	{
		var items = [];
		this.iterator(function(index, value)
		{
			if(angular.isDefined(value[column]))
			{
				items.push({index: index, value: value[column]});
			}
		});
		return items;
	},

	columnValueExits: function(column, value) {
		var items = this.column(column);

		for(var i in items) {
			if(items[i].value == value){
				return true;
			}
		}
	},

	countColumnItems: function(column, value) {
		var items = this.column(column);
		var j = 0;
		for(var i in items) {

			if(items[i].value === value){
				j++;
			}
		}
		return j;
	},

	/**
	 * Sum
	 *
	 * @param  string column
	 * @return array
	 */
	sum: function (column)
	{
		var sum = 0, rows = this.column(column);
		for(var i in rows) {
			if(angular.isNumber(rows[i])) {
				sum += rows[i];
			}
		}
		return sum;
	},

	/**
	 * Column Array
	 *
	 * @param  string column
	 * @return array
	 */
	columnArray: function (column)
	{
		var array = [], rows = this.column(column);
		for(var i in rows) {
			if(angular.isNumber(rows[i])) {
				array.push( parseFloat(rows[i]) );
			}
		}
		return array;
	},

	/**
	 * Max
	 *
	 * @param  string column
	 * @return array
	 */
	max: function (column)
	{
		return Math.max.apply(Math,this.columnArray(column));
	},

	/**
	 * Min
	 *
	 * @param  string column
	 * @return array
	 */
	min: function (column)
	{
		return Math.min.apply(Math,this.columnArray(column));
	},

	/**
	 * Sum
	 *
	 * @param  string column
	 * @return array
	 */
	average: function (column)
	{
		return (this.sum(column)/this.columnArray(column).length);
	},

	/**
	 * Get and set storage
	 *
	 * @param  string column
	 * @return array
	 */
	storage: function(key, value)
	{
		if(angular.isString(key))
		{
			if(angular.isDefined(value))
			{
				return this.store[key] = value;
			}
			else
			{
				return this.store[key];
			}
		}
		return false;
	},

	/**
	 * Percentage
	 *
	 * This is potentially expensive
	 *
	 * @param  string  column
	 * @param  integer value
	 * @return array
	 */
	percentage: function (column, value)
	{
		sum = this.sum(column);
		if(angular.isNumber(value) && angular.isNumber(sum))
		{
			return ((value/sum)*100).toFixed(2);
		}
		return 0;
	},

	/**
	 * Within - find the absolute value that exists between a maxima,minima
	 *
	 * @param  integer number
	 * @param  integer min
	 * @param  integer max
	 * @return integer
	 */
	within: function(n, min, max)
	{
		n = parseInt(n);
		max = parseInt(max);
		min = parseInt(min);

		if(n < min) return min;
		if(n > max) return max;
		return n;
	},

	hydrated: {},
	hydrateBlacklist: ['$id', '$selected', '$$hashKey'],

	/**
	 * Add blacklist items
	 *
	 * @param array items
	 * @return object
	 */
	addBlacklistItems: function(items)
	{
		for(var i in items)
		{
			this.hydrateBlacklist.push(items[i]);
		}
	},


	/**
	 * Hydrate the object. Clean it and normalise it to a nested array/object.
	 *
	 * @param object
	 * @return object
	 */
	cleanItem: function(object)
	{
		var collect = {};
		for(var j in object)
		{
			if(this.hydrateBlacklist.indexOf(j) == -1 )
			{
				collect[j] = object[j];
			}
		}

		return collect;
	},

	/**
	 * Hydrate the object. Clean it and normalise it to a nested array/object.
	 *
	 * @param object
	 * @return object
	 */
	hydrate: function(object)
	{
		var object = angular.isDefined(object)? object: this.get();
		var hydrate = {};

		for(var i in object)
		{
			if(angular.isDefined(object[i]) && (angular.isObject(object[i]) || angular.isArray(object[i])))
			{
				hydrate[i] = {};
				for(var j in object[i])
				{
					if(this.hydrateBlacklist.indexOf(j) == -1 )
					{
						if(angular.isArray(object[i][j])) {
							hydrate[i][j] = this.hydrate(object[i][j]);
						}
						else hydrate[i][j] = object[i][j];
					}
				}
			}
		}
		return hydrate;
	},

	/**
	 * Serialise
	 *
	 * @return array
	 */
	serialise: function()
	{
		return this.hydrate();
	},

	/**
	 * Toggle the select state
	 *
	 * @return void
	 */
	toggleSelect: function ()
	{
		this.bulkApply('$selected', (this.selected = this.selected? false: true));
	},

	/**
	 * Bulk apply
	 *
	 * @return void
	 */
	bulkApply: function (column, value)
	{
		for(var i in this.origin)
		{
			if(angular.isDefined(this.origin[i][column])) {
				this.origin[i][column] = value;
			}
		}
	},


	/**
	 * Exclusively only select one row
	 *
	 * @return boolean
	 */
	exclusiveSelect: function (id)
	{
		this.bulkApply('$selected', (this.selected = false));
		var passed = false;
		this.iterator(function(index, row) {
			if(row.$id === id) {
				row.$selected = true;
				passed = id;
			}
		}, this.origin);
		return passed;
	},

	/**
	 * Order by
	 *
	 */
	order: function (column, direction)
	{
		if(angular.isString(column))
		{
			var orderKey  = 'order::' + column;

			if(!angular.isDefined(direction))
			{
			   direction = this.storage(orderKey)? this.storage(orderKey, false): this.storage(orderKey, true);
			}

			return this.origin.sort(function(a, b) {
				if (a.properties[column] < b.properties[column])
					return !!direction? -1: 1;
				if (a.properties[column] > b.properties[column])
					return !!direction? 1: -1;
				return 0;
			});
		}
	}
});