
return function(properties, collection)
{

	return {

		/**
		 * @param mix
		 */
		properties: properties,

		/**
		 * Finding index not most efficient
		 * Could delete on parent
		 *
		 * @return mix
		 */
		index: function()
		{
			var mine = this.get('$index');
			for(i in collection.items.origin) {
				if(collection.items.origin[i].get('$index') == mine) return i;
			}
			return null;
		},

		/**
		 * Remove
		 *
		 * @return bool
		 */
		remove: function()
		{
			return collection.removeById(this.index());
		},

		/**
		 * Get a property
		 *
		 * @param  string string
		 * @return mixed
		 */
		get: function(property)
		{
			if(angular.isDefined(this.properties[property])) {
				return this.properties[property];
			}
			return null;
		},

		/**
		 * Property exists
		 *
		 * @param  string string
		 * @return bool
		 */
		propertyExists: function(property)
		{
			return (angular.isDefined(this.properties[property]));
		},

		/**
		 * Update
		 *
		 * @param  string string
		 * @return mixed
		 */
		update: function(updates)
		{
			if(angular.isObject(updates))
			{
				for(var i in updates)
				{
					if(this.propertyExists(i))
					{
						this.properties[property] = updates[i];
					}
				}
				return true;
			}
			return false;
		},

		/**
		 * Move item up or down list
		 *
		 * @param step
		 */
		move: function(step)
		{
			return collection.move(this.index(), parseInt(step));
		}
	}
}