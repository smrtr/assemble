/*
 * @inject: $scope
 */

$scope.confirm = function () {
	$scope.$close(true);
};

$scope.close = function () {
	$scope.$close(false);
};