/* @inject: $log*/

var obj = {

	/**
	 * Object Value
	 *
	 * @param string
	 * @param object
	 */
	objectValue: function(str, obj) {

		var srcMap = angular.extend({}, obj);
		var segments = str.split('.');

		for(var i in segments) {

			var segment = segments[i];

			if(!angular.isDefined(srcMap[segment])) {
				return false;
			} else {
				srcMap = srcMap[segment];
			}
		}
		return srcMap;
	}

};

return obj;