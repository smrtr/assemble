/* @inject: $log*/



	this.concat = function(array, separator) {
		var sep = separator || '';

		if(angular.isArray(array)) {
			return array.join(sep)
		}
		return $log.error('Argument 1 must be an array');
	}

	/**
	 * Reverse
	 *
	 * @param string string
	 */
	this.reverse = function(str) {
		return str.split("").reverse().join("");
	}

	/**
	 * String to slug
	 *
	 * @param string string
	 */
	this.toSlug = function(str, sep) {
		return __slugify(str, sep);
	}

	/**
	 * Swap tokens for values inside template
	 *
	 * @param string template
	 * @param object tokens
	 * @param string tokenWrap
	 */
	this.template = function(template, tokens, tokenWrap) {
		var startTag = tokenWrap || '#', endTag = str.reverse(startTag);
		if(angular.isString(template)) {
			if(template && angular.isObject(tokens)) {
				for(var i in tokens) {
					token = str.concat([startTag, i, endTag]);

					if(angular.isDefined(tokens[i])) {
						template = template.replace(new RegExp(token, 'g'), tokens[i]);
					}
				}
			}
		}
		return template;
	}

	this.serialise = function(str) {
		if(angular.isString(str)) {
			try {
				JSON.parse(str);
			}
			catch(e) {
				console.log('ERROR: COULD NOT CONVERT JSON SCHEMA');
				console.log(str);
				throw 'Unable to convert Form data';

			}
		}else {
			str = '{}';
		}
		return JSON.parse(str); // need to fall back for this

	}

	/**
	 * To Json Object
	 *
	 * @param string string
	 */
	this.toJson = function(str) {
		return this.serialise(str);
	}