/* @inject: $log*/

var str = {

	concat: function(array, separator) {
		var sep = separator || '';

		if(angular.isArray(array)) {
			return array.join(sep)
		}
		return $log.error('Argument 1 must be an array');
	},

	/**
	 * Reverse
	 *
	 * @param string string
	 */
	reverse: function(string) {
		return string.split("").reverse().join("");
	},

	/**
	 * String to slug
	 *
	 * @param string string
	 */
	toSlug: function(string, sep) {
		return __slugify(string, sep);
	},

	/**
	 * Swap tokens for values inside template
	 *
	 * @param string template
	 * @param object tokens
	 * @param string tokenWrap
	 */
	template: function(template, tokens, tokenWrap) {
		var startTag = tokenWrap || '#', endTag = str.reverse(startTag);
		if(angular.isString(template)) {
			if(template && angular.isObject(tokens)) {
				for(var i in tokens) {
					token = str.concat([startTag, i, endTag]);

					if(angular.isDefined(tokens[i])) {
						template = template.replace(new RegExp(token, 'g'), tokens[i]);
					}
				}
			}
		}
		return template;
	},

	serialise: function(str) {
		try {
			JSON.parse(str)
		}
		catch(e) {
			console.log('ERROR: COULD NOT CONVERT JSON SCHEMA');
			//$rootScope.dialog('error', {message:'error_parsing_form_schema'})
			return {}
		}
		return JSON.parse(str); // need to fall back for this
	}



};

return str;