/**
 * @inject: $scope, ResourceManagerServer as RMS
 * @route: /resource; list all
 * @route: /resource/:id; Manage single resource
 * @name: renameThisControllerToSomething else
 * @templateUrl: this/is/the/template.html
 * @layout: default
 * @state: state:assets.service-routines.list
 * @state: abstract:true
 */
$scope.title = 'home';
