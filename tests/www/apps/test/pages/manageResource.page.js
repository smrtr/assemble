/**
 * @inject: $scope, ResourceManagerServer as RMS
 * @route: /resource; list all
 * @route: /resource/:id; Manage single resource
 * @name: renameThisControllerToSomething else
 * @templateUrl: this/is/the/template.html
 * @layout: default
 * @state: state:assets.service-routines.list
 * @state: abstract:true
 * @annotation: ({
 *	inject: [$scope, 'ResourceManagerServer as RMS'],
 *	route:[/resource, /resource/:id],
 *	resolve: {
 *		test: function() {
 *			return 1;
 *		}
 *	}
 * })
 */
$scope.title = 'home';
