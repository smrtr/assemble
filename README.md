

[ ![Codeship Status for smrtr/Assemble](https://codeship.com/projects/58cac6e0-c1ab-0132-6a63-66e425bd3834/status?branch=master)](https://codeship.com/projects/73590)

# Assemble

*PHP Server side Angular app compiler.*


### What is it?
A server side angular app compiler.

### Why was it made?
To primarily increase developer productivity.

### How?
- Enforcing best practice conventions through the naming of files and structuring of folders
- Easy maintenance of dependencies and other configurations through Assemble Annotations
- Automatic processing and inclusion of app routes
- Automatic inclusion of dialogs
- Automatic inclusion of templates

### Other benefits?
There were also some potential performance advantages including:
crafted scripts per request
and the non download of kitchen sink

## How does it work?
You tell angular assemble where to find the app directory. Assemble then recursively scans, parses and configures the scripts into one single `app.js` script that is to be downloaded by the browser. 

The rest is up to angular, any additional files that angular needs whilst the app is running must be http accessible.

Assemble has a few files that each have their roles:-

- **Angular** - scans app and looks for js files and outputs the app script.
- **File** - parses a file, extracts annotations, configures naming strategies
- **Provider** - Angular container manager, builds a provider container based upon Assemble\File.
- **Templates** look for .tpl.html files and seek to preload them for later use. (it's manually done at the moment I.e. Not included as part of Assemble\Angular)

## Improvements
- Combine templates in the build process to form a templates.js resource
- Caching to prevent re-scanning
- port to nodejs and run as a gulp task, proceed to minification to reduce performance costs.

## Test plan
- Test how file names are converted via the naming strategy
- Test extraction of annotations/ single and multiline 
- Test the setting of provider based upon filename (naming strategy)
- Test that the correct layout URL is produced
- Test that files with prefix _ are skipped.
- Test that debug mode prints path of script inside container
- Test template naming strategy (snake case)
- Test that @name annotation overrides naming strategy
- Test that controllers are camel case 
- Test that hooks/callbacks allow piping of CRUDing of scripts
- Test that module dependency is injected
- Can assemble be given an app directory which is not inside the public folder , e.g. in the src folder
- Can assemble function without implementing header calls, i.e. is it context-sensitive? (Currently YES)

### Other questions to answer

- What scenarios and inputs does assembler accept?
- What outputs and effects does assembler produce?
- Are these implementations testable?
- Any assumptions?




#Implementation

	$configs = array(
		'app' => 'app',
		'path' => 'public'
	);
	$app = new Smrtr\Assemble\Angular($configs, $hooks);
	$app->output();

	//outputs 
	angular.module('app', []);

Given a file structure of
	
	public/
		index.php
		app/
			common/
				pages/
					404.page.js
					404.page.view.html
			modules/
				hello/
					pages/
						world.page.js
						world.page.view.js





	$configs = array(
		'app' => 'app',
		'path' => 'public'
	);

These declartions could be made easier:

	$callbacks = array(

		function(Smrtr\Assemble\Angular &$angular) {

			if($angular->has('dialogs'))
			{
				$provider = new Smrtr\Assemble\Provider(array(
					'module'    => 'common',
					'namespace' => 'core', 
					'baseurl'   => $angular->baseurl,
					'base'		=> 'path/to/public',
					'path'      => 'path/to/public/shared/configs/_dialog.run.js'), //mock a path 
					'app'       => $angular->app
				));

				$angular->push('packages', 'ui.bootstrap');
				return $provider->toString(
					"var dialogs = ".json_encode($angular->get('dialogs'))."\n".
					$this->safelyInclude($this->application->path('project_public', $angular->app . '/shared/configs/_dialog.run.js'), 'require', true)
				);
			}
		}
	);


	$app = new Smrtr\Assemble\Angular($configs, $callbacks);
	$app->output();

	//outputs 
	angular.module('app', []);